﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Enums;

namespace GameBrain
{
    public class Ship
    {
        public int ShipId { get; set; }
        public string Name { get; set; } = null!;
        public int Size { get; set; }

        public Position? AnchorPosition { get; set; } 
        
        public ELayout Layout { get; set; }

        public bool IsSunken { get; set; } = false;

        public Ship(string name, int size)
        {
            Size = size;
            Name = name;
        }

        public Ship()
        {
        }

        public Ship(Domain.Ship dbShip)
        {
            ShipId = dbShip.ShipId;
            Name = dbShip.Name;
            Size = dbShip.Size;
        }

        public Ship(Ship ship, int r1, int c1, ELayout layout)
        {
            ShipId = ship.ShipId;
            Name = ship.Name;
            Size = ship.Size;
            AnchorPosition = new Position(r1, c1);
            Layout = layout;
        }

        public List<Position> CalculateShipCells()
        {
            var location = new List<Position>();
            
            if (AnchorPosition == null)
            {
                return location;
            }

            for (int i = 0; i < Size; i++)
            {
                location.Add(
                    Layout == ELayout.Horizontal
                    ? new Position(AnchorPosition.Row, AnchorPosition.Column + i)
                    : new Position(AnchorPosition.Row + i, AnchorPosition.Column)
                    );
            }

            return location;
        }

        public void SwitchLayout()
        {
            Layout = Layout == ELayout.Horizontal ? ELayout.Vertical : ELayout.Horizontal;
        }

        public void SwitchLayout(int rows, int cols)
        {
            if (AnchorPosition == null) return;

            Layout = Layout == ELayout.Horizontal ? ELayout.Vertical : ELayout.Horizontal;

            var isOnBoard = HasCorrectLocation(rows, cols);

            if (!isOnBoard)
            {
                var invalidPosCount = CalculateShipCells().Sum(position => position.IsOnBoard(rows, cols) ? 0 : 1);

                if (Layout == ELayout.Horizontal) AnchorPosition.Column -= invalidPosCount;
                else AnchorPosition.Row -= invalidPosCount;
            }
        }

        public bool MoveAnchorPosition(int rows, int cols, int delta, bool isMoveOnXAxis)
        {
            if (AnchorPosition == null) return false;
            Action redoAction;
            if (isMoveOnXAxis)
            {
                AnchorPosition.Column += delta;
                redoAction = () => AnchorPosition.Column -= delta;
            }
            else
            {
                AnchorPosition.Row += delta;
                redoAction = () => AnchorPosition.Row -= delta;
            }

            var isOnBoard = HasCorrectLocation(rows, cols);

            if (!isOnBoard) redoAction.Invoke();

            return isOnBoard;
        }

        private bool HasCorrectLocation(int boardRows, int boardCols)
        {
            var shipCells = CalculateShipCells();
            var isOnBoard = true;
            var i = 0;
            while (isOnBoard && i < shipCells.Count)
            {
                isOnBoard = shipCells[i].IsOnBoard(boardRows, boardCols);
                i++;
            }

            return isOnBoard;
        }

        public override bool Equals(object obj)
        {
            if (GetType() != obj.GetType())
            {
                return false;
            }

            Ship other = (Ship) obj;
            return Name == other.Name && Size == other.Size;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Name, Size);
        }

        public override string ToString()
        {
            return $"{Name} (1x{Size})";
        }
    }
}