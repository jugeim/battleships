﻿using System;

namespace GameBrain.Exceptions
{
    public class GameEndException : Exception
    {
        public GameEndException(string message) : base(message)
        {
        }
    }
}