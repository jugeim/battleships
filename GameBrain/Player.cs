﻿using System;
using System.Linq;
using AI;
using Domain.Enums;

namespace GameBrain
{
    public class Player
    {
        public int PlayerId { get; set; }
        public string Name { get; set; }
        
        public EPlayerType PlayerType { get; set; }
        
        public Board Board { get; set; }
        
        private RegularAi Ai { get; set; } = null!;

        public Player(string name, Board board, GameSessionParams sessionParams)
        {
            Name = name;
            PlayerType = EPlayerType.AI;
            Ai = new RegularAi(board.Rows, board.Columns,
                sessionParams.ShipsCanTouch,
                sessionParams.GameSessionShips
                    .Select(pair => (pair.Key.Size, pair.Value))
                    .GroupBy(
                        pair => pair.Size,
                        pair => pair.Value,
                        (size, values) => new {Key = size, Count = values.Sum()}
                    )
                    .ToDictionary(pair => pair.Key, pair => pair.Count)
                );
            
            Board = board;
        }

        public Player(string name, Board board)
        {
            Name = name;
            PlayerType = EPlayerType.Human;
            Board = board;
        }

        public Player()
        {
            Name = "Player";
            Board = new Board(10,10);
        }

        public Player(Domain.Player dbPlayer)
        {
            
            PlayerId = dbPlayer.PlayerId;
            Name = dbPlayer.Name;
            PlayerType = dbPlayer.PlayerType;
            Board = new Board(dbPlayer.Board);
        }

        public RegularAi GetPlayerAi()
        {
            if (PlayerType != EPlayerType.AI) throw new Exception("Human dont have an AI");
            if (Ai == null) throw new Exception("AI was not set. Should find out, why");
            return Ai;
        }

        public void SetPlayerAi(RegularAi ai)
        {
            Ai = ai;
        }

        public (int row, int col) GiveMoveCoordinates()
        {
            if (PlayerType != EPlayerType.AI) throw new Exception("Should not use this function with human players");

            return Ai.GiveMoveCoordinates();
        }

        public Domain.Player SerializePlayer()
        {
            var player = new Domain.Player
            {
                PlayerType = PlayerType,
                Name = Name,
            };
            
            return player;
        }
        
    }
}