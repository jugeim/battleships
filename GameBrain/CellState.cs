﻿namespace GameBrain
{
    public enum CellState
    {
        Empty,
        Ship,
        Miss,
        Hit,
        Error
    }
}