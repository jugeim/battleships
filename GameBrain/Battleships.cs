﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using AI;
using Domain;
using Domain.Enums;
using GameBrain.Exceptions;
using GameBrain.Serialization;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace GameBrain
{
    public class Battleships
    {
        private int _gameId;

        public int GameId
        {
            get => _gameId;
            set
            {
                if (GameId <= 0)
                {
                    _gameId = value;
                }
            }
        }

        public Player PlayerA {get; private set; }

        public Player PlayerB {get; private set; }

        public bool NextMoveByA { get; private set; } = true;

        public (int Height, int Width) BoardDimensions { get; private set; }

        public string GameName { get; private set; }
        public int GameOptionId { get; private set; }
        public string GameOptionName { get; private set; }
        
        public EShipsCanTouch ShipsCanTouch { get; private set; }
        public ENextMoveAfterHit NextMoveAfterHit { get; private set; }
        
        public Dictionary<Ship, int> GameSessionShips { get; private set; }

        public Battleships()
        {
            PlayerA = new Player();
            PlayerB = new Player();
            GameName = "Battleships";
            GameOptionName = "none";
            GameSessionShips = new Dictionary<Ship, int>();
        }

        public Battleships(GameSessionParams sessionParams)
        {
            GameName = sessionParams.GameName;
            GameOptionId = sessionParams.GameOptionId;
            GameOptionName = sessionParams.GameOptionName;
            BoardDimensions = (sessionParams.BoardHeight, sessionParams.BoardWidth);
            var boardA = new Board(sessionParams.BoardHeight, sessionParams.BoardWidth);
            var boardB = new Board(sessionParams.BoardHeight, sessionParams.BoardWidth);
            
            PlayerA = sessionParams.PlayerAType == EPlayerType.AI 
                ? new Player(sessionParams.PlayerAName, boardA, sessionParams) 
                : new Player(sessionParams.PlayerAName, boardA);
            
            PlayerB = sessionParams.PlayerBType == EPlayerType.AI 
                ? new Player(sessionParams.PlayerBName, boardB, sessionParams)
                : new Player(sessionParams.PlayerBName, boardB);

            ShipsCanTouch = sessionParams.ShipsCanTouch;
            NextMoveAfterHit = sessionParams.NextMoveAfterHit;

            GameSessionShips = sessionParams.GameSessionShips;
        }

        public Player? GetWinner()
        {
            if (PlayerB.Board.HasLoosed() && PlayerA.Board.HasLoosed())
                throw new Exception("Game was not stopped at the right time, so both players loosed. Sorry");
            if (PlayerA.Board.HasLoosed()) return PlayerA;
            return PlayerB.Board.HasLoosed() ? PlayerB : null;
        }

        public bool PlaceBomb(int row, int col)
        {
            var attackedPlayer = NextMoveByA ? PlayerB : PlayerA;
            try
            {
                return attackedPlayer.Board.PlaceBomb(row, col);
            }
            catch (GameEndException)
            {
                var leader = !NextMoveByA ? PlayerB : PlayerA;
                throw new GameEndException($"{leader.Name} has won!");
            }
        }

        public void SwitchPlayer()
        {
            NextMoveByA = !NextMoveByA;
        }

        public Player GetCurrentPlayer()
        {
            return NextMoveByA ? PlayerA : PlayerB;
        }

        public Ship PlaceShip(Ship ship, int row1, int col1, ELayout layout)
        {
            return NextMoveByA
                ? PlayerA.Board.PlaceShip(ship, row1, col1, layout)
                : PlayerB.Board.PlaceShip(ship, row1, col1, layout);
        }

        public GameSessionPublicParameters GetPublicSessionParametersForAiPlayer()
        {
            var opponentsBoard = NextMoveByA ? PlayerB.Board : PlayerA.Board;
            return new GameSessionPublicParameters
            {
                BoardHeight = BoardDimensions.Height,
                BoardWidth = BoardDimensions.Width,
                ShipsCanTouch = ShipsCanTouch,
                GameShips = GameSessionShips
                    .Select(pair => (pair.Key.Size, pair.Value))
                    .GroupBy(
                        pair => pair.Size, 
                        pair => pair.Value,
                        (size, values) => new { Key = size, Count = values.Sum() }
                    )
                    .ToDictionary(pair => pair.Key, pair => pair.Count),
                PlacedBombs = opponentsBoard.BombCells.Select(pos => (pos.Row, pos.Column)).ToHashSet()
            };
        }

        public string GetSerializedGameState()
        {
            var state = new GameState();
            state.GameId = GameId;
            state.GameName = GameName;
            state.PlayerA =  PlayerA;
            state.PlayerB = PlayerB;
            state.NextMoveByA = NextMoveByA;
            state.GameOptions = new SerializedGameOptions
            {
                GameOptionId = GameOptionId,
                GameOptionName = GameOptionName,
                BoardHeight = BoardDimensions.Height,
                BoardWidth = BoardDimensions.Width,
                NextMoveAfterHit = NextMoveAfterHit,
                ShipsCanTouch = ShipsCanTouch,
                GameSessionShips = GameSessionShips
                    .Select(pair => new SerializedGameOptions.ShipAmountPair(pair.Key, pair.Value)).ToArray()
            };
            var jsonOptions = new JsonSerializerOptions()
            {
                WriteIndented = true
            };
            return JsonSerializer.Serialize(state, jsonOptions);
        }

        public void SetGameStateFormJsonString(string jsonString)
        {
            var jsonOptions = new JsonSerializerOptions()
            {
                WriteIndented = true
            };
            var state = JsonSerializer.Deserialize<GameState>(jsonString, jsonOptions);

            if (state == null) throw new Exception("Error on JSON deserialization");

            GameId = state.GameId;
            GameName = state.GameName;
            NextMoveByA = state.NextMoveByA;
            PlayerA = state.PlayerA;
            PlayerB = state.PlayerB;
            GameOptionId = state.GameOptions.GameOptionId;
            GameOptionName = state.GameOptions.GameOptionName;
            BoardDimensions = (state.GameOptions.BoardHeight, state.GameOptions.BoardWidth);
            NextMoveAfterHit = state.GameOptions.NextMoveAfterHit;
            ShipsCanTouch = state.GameOptions.ShipsCanTouch;
            GameSessionShips = state.GameOptions.GameSessionShips
                .ToDictionary(pair => pair.Ship, pair => pair.Amount);
            
            if (PlayerA.PlayerType == EPlayerType.AI) PlayerA.SetPlayerAi(new RegularAi());
            if (PlayerB.PlayerType == EPlayerType.AI) PlayerB.SetPlayerAi(new RegularAi());
        }
        
        public void SetGameStateFromDbData(Domain.Game gameState)
        {
            GameId = gameState.GameId;
            GameName = gameState.Name;
            GameOptionId = gameState.GameOptionId;
            GameOptionName = gameState.GameOption.Name;
            PlayerA = gameState.PlayerA.PlayerId > 0 ? new Player(gameState.PlayerA) : new Player();
            PlayerB = gameState.PlayerB.PlayerId > 0 ? new Player(gameState.PlayerB) : new Player();
            NextMoveByA = gameState.NextMoveByA;
            BoardDimensions = (gameState.GameOption.BoardHeight, gameState.GameOption.BoardWidth);
            ShipsCanTouch = gameState.GameOption.ShipsCanTouch;
            NextMoveAfterHit = gameState.GameOption.NextMoveAfterHit;
            GameSessionShips = gameState.GameOption.GameOptionShips
                .Select(optShip => (new Ship(optShip.Ship), optShip.Amount))
                .ToDictionary(pair => pair.Item1,
                    pair => pair.Amount);
            
            if (PlayerA.PlayerType == EPlayerType.AI) PlayerA.SetPlayerAi(new RegularAi());
            if (PlayerB.PlayerType == EPlayerType.AI) PlayerB.SetPlayerAi(new RegularAi());
        }

        public GameOption GetSessionOptions()
        {
            var options = new GameOption
            {
                NextMoveAfterHit = NextMoveAfterHit,
                ShipsCanTouch = ShipsCanTouch,
                BoardHeight = BoardDimensions.Height,
                BoardWidth = BoardDimensions.Width,
                Name = GameOptionName
            };
            if (GameOptionId > 0)
            {
                options.GameOptionId = GameOptionId;
            }

            return options;
        }
    }
}