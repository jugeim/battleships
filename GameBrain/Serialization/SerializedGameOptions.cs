﻿using System.Collections.Generic;
using Domain.Enums;

namespace GameBrain.Serialization
{
    public class SerializedGameOptions
    {
        public class ShipAmountPair
        {
            public Ship Ship { get; set; } = null!;
            public int Amount { get; set; }

            public ShipAmountPair()
            {
                
            }

            public ShipAmountPair(Ship ship, int amount)
            {
                Ship = ship;
                Amount = amount;
            }
        }
        
        public int GameOptionId { get; set; }
        public string GameOptionName { get; set; } = "";
        
        public int BoardHeight  { get; set; }
        
        public int BoardWidth  { get; set; }


        public EShipsCanTouch ShipsCanTouch { get; set; }
        public ENextMoveAfterHit NextMoveAfterHit { get; set; }
        
        public ShipAmountPair[] GameSessionShips { get; set; } = new ShipAmountPair[0];

        public SerializedGameOptions()
        {
            
        }
    }
}