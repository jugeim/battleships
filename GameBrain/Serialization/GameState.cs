﻿namespace GameBrain.Serialization
{
    public class GameState
    {
        public int GameId { get; set; }

        public string GameName { get; set; } = "";
        
        public SerializedGameOptions GameOptions { get; set; } = new SerializedGameOptions();

        public bool NextMoveByA { get; set; }
        
        public Player PlayerA {get; set; } = null!;
         
        public Player PlayerB {get; set; } = null!;

        public GameState()
        {
            
        }
        
    }
    
    
}