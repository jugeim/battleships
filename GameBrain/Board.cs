﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Enums;
using GameBrain.Exceptions;

namespace GameBrain
{
    public class Board
    {
        public int BoardId { get; set; }
        public int Rows { get; set; }
        public int Columns { get; set; }

        public List<Ship> Ships { get; set; } = new List<Ship>();
        public List<Position> AllShipCells() => Ships.SelectMany(ship => ship.CalculateShipCells()).ToList();

        public bool HasLoosed() => Ships.TrueForAll(s => s.IsSunken);

        public List<Position> BombCells { get; set; } = new List<Position>();
        
        public Board() // Used in JSON deserialization
        {
        }

        public Board(int rows, int columns)
        {
            Rows = rows;
            Columns = columns;
        }

        public Board(Domain.Board dbBoard)
        {
            BoardId = dbBoard.BoardId;
            Rows = dbBoard.Rows;
            Columns = dbBoard.Columns;
             BombCells = dbBoard.Bombs.Select((bomb, order)=> (new Position(bomb.Row, bomb.Column), order))
                    .OrderBy(pair => pair.order) // to be sure that list is ordered by bomb placing order 
                    .Select(pair => pair.Item1)
                    .ToList();
            Ships = dbBoard.ShipsOnBoard.Select(shipOnBoard => new Ship
            {
                ShipId = shipOnBoard.ShipId,
                Name = shipOnBoard.Ship.Name,
                Size = shipOnBoard.Ship.Size,
                AnchorPosition = new Position(shipOnBoard.StartRow, shipOnBoard.StartColumn),
                Layout = shipOnBoard.Layout,
                IsSunken = shipOnBoard.IsSunken
            }).ToList();
        }

        public CellState GetCellState(int row, int col)
        {
            var pos = new Position(row, col);
            var hasShip = AllShipCells().Contains(pos);
            var hasMultipleShips = AllShipCells().FindAll(p => p.Equals(pos)).Count > 1;
            var hasBomb = BombCells.Contains(pos);
            
            return hasMultipleShips || row >= Rows || col >= Columns ? CellState.Error 
                : hasBomb && hasShip ? CellState.Hit 
                : hasShip ? CellState.Ship 
                : hasBomb ? CellState.Miss 
                : CellState.Empty;
        }
        private bool CheckCoordinates(int row, int col)
        {
            return row >= 0 && row < Rows && col >= 0 && col < Columns;
        }

        
        /**
         * return true if bomb hit the ship
         * throw GameEndException if no ships left
         * throw ArgumentException if coordinates already have been bombed
         */
        public bool PlaceBomb(int row, int col)
        {
            if (!CheckCoordinates(row, col)) throw new ArgumentException("Invalid Coordinates");
            
            var pos = new Position(row, col);

            if (BombCells.Contains(pos))
            {
                throw new ArgumentException("Cell was already checked");
            }
            BombCells.Add(pos);
            var hasHit = AllShipCells().Contains(pos);
            if (hasHit)
            {
                var hitShip = Ships.Find(ship => ship.CalculateShipCells().Contains(pos));
                hitShip.IsSunken = hitShip.CalculateShipCells().TrueForAll(cell => BombCells.Contains(cell));
            }

            if (Ships.TrueForAll(ship => ship.IsSunken))
            {
                throw new GameEndException("Game end");
            }
            return hasHit;
        }

        public Ship PlaceShip(Ship ship, int r1, int c1, ELayout layout)
        {
            var shipOnBoard = new Ship()
            {
                Name = ship.Name,
                Size = ship.Size,
                AnchorPosition = new Position(r1, c1),
                Layout = layout
            };
            
            Ships.Add(shipOnBoard);

            return shipOnBoard;
        }
        
        public Ship PlaceShip(Ship ship)
        {
            
            Ships.Add(ship);

            return ship;
        }

        public bool CanShipBePlaced(Ship? ship, bool isShipOnCellAllowed, EShipsCanTouch placingRule)
        {
            if (ship == null) return false;
            var canBePlaced = true;
            var shipCells = ship.CalculateShipCells();
            var i = 0;
            while (canBePlaced && i < shipCells.Count)
            {
                Position cell = shipCells[i];
                var cellState = GetCellState(cell.Row, cell.Column);
                canBePlaced = cellState != CellState.Error && (isShipOnCellAllowed || cellState != CellState.Ship);
                var allShipCells = AllShipCells();
                if (isShipOnCellAllowed) // ship was "placed" on board to ship its in game view, so should remove ship cells from all ship cells
                {
                    var withoutShip = allShipCells.Except(shipCells).ToList();
                    shipCells.ForEach(sc =>
                    {
                        if (allShipCells.Count(c => c.Equals(sc)) > 1) withoutShip.Add(sc);
                    });
                    allShipCells = withoutShip;
                }
                canBePlaced = canBePlaced && (placingRule == EShipsCanTouch.Yes 
                                              || cell.GetSidePositionsOnBoard(Rows, Columns)
                                                  .TrueForAll(spos => !allShipCells.Contains(spos)) // allow corners
                                              && (placingRule == EShipsCanTouch.Corner // if corners can touch
                                                  || placingRule == EShipsCanTouch.No // else 
                                                  && cell.GetCornerPositionsOnBoard(Rows, Columns)
                                                      .TrueForAll(spos => !allShipCells.Contains(spos))) // no ship cell around allowed
                                            );
                                  
                i++;
            }

            return canBePlaced;
        }

        public bool MoveLastShip(string direction, out int row, out int column, EShipsCanTouch placingRule)
        {
            var lastShip = Ships.Last();

            var shouldChangeLayout = direction == "s";
            var delta = shouldChangeLayout ? 0 : direction == "r" || direction == "d" ? 1 : -1;
            var isXAxis = direction == "l" || direction == "r";
            
            if (shouldChangeLayout) 
                lastShip.SwitchLayout(Rows, Columns);
            else 
                lastShip.MoveAnchorPosition(Rows, Columns, delta, isXAxis);
            
            (row, column) = lastShip.AnchorPosition?.GetCoordinates() ?? (0, 0);
            
            return CanShipBePlaced(lastShip, true, placingRule);
        }

        public Ship? RemoveLastShip()
        {
            var ship = Ships.Count > 0 ? Ships.Last() : null;
            if (ship != null) Ships.RemoveAt(Ships.Count - 1);
            return ship;
        }

        public Domain.Board SerializeBoard(Domain.Ship[] ships)
        {
            var board = new Domain.Board
            {
                Rows = Rows,
                Columns = Columns,
                ShipsOnBoard = Ships.Select((ship, i) => new ShipOnBoard
                {
                    ShipId = ships.First(s => s.Name == ship.Name && s.Size == ship.Size).ShipId,
                    StartRow = ship.AnchorPosition!.Row,
                    StartColumn = ship.AnchorPosition!.Column,
                    Layout = ship.Layout,
                    IsSunken = ship.IsSunken
                }).ToArray(),
                Bombs = BombCells.Select((bombCell, i) => new Bomb
                {
                    Row = bombCell.Row,
                    Column = bombCell.Column,
                    Order = i
                }).ToArray()
            };

            return board;
        }
    }
}