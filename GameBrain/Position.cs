﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GameBrain
{
    public class Position
    {
        public int Row { get; set; }
        public int Column { get; set; }

        public Position()
        {
            
        }

        public Position(int row, int column)
        {
            Row = row;
            Column = column;
        }

        public (int, int) GetCoordinates()
        {
            return (Row, Column);
        }

        public List<Position> GetCornerPositionsOnBoard(int boardRows, int boardCols)
        {
            return new List<Position>
            {
                new Position(Row - 1, Column - 1),
                new Position(Row - 1, Column + 1),
                new Position(Row + 1, Column - 1),
                new Position(Row + 1, Column + 1)
            }
                .Where(pos => pos.IsOnBoard(boardRows, boardCols))
                .ToList();
        }

        public List<Position> GetSidePositionsOnBoard(int boardRows, int boardCols)
        {
            return new List<Position>
            {
                new Position(Row, Column - 1),
                new Position(Row, Column + 1),
                new Position(Row - 1, Column),
                new Position(Row + 1, Column)
            }
                .Where(pos => pos.IsOnBoard(boardRows, boardCols))
                .ToList();
            
        }

        public bool IsOnBoard(int rows, int cols)
        {
            return Row > -1 && Row < rows && Column > -1 && Column < cols;
        }

        public override bool Equals(object obj)
        {
            //Check for null and compare run-time types.
            if (GetType() != obj.GetType())
            {
                return false;
            }
            else {
                Position other = (Position) obj;
                return (Row == other.Row) && (Column == other.Column);
            }
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Row, Column);
        }
    }
}