﻿using System.Collections.Generic;
using Domain.Enums;

namespace GameBrain
{
    public class GameSessionParams
    {
        public static readonly List<Ship> DefaultShips = new List<Ship>()
        {
            new Ship("Carrier", 5),
            new Ship("Battleship", 4),
            new Ship("Cruiser", 3),
            new Ship("Submarine", 3),
            new Ship("Destroyer", 2)
        };

        public string GameName { get; set; }

        public int GameOptionId { get; set; }
        public string GameOptionName { get; set; }

        public int BoardHeight { get; set; }
        public int BoardWidth { get; set; }

        public string PlayerAName { get; set; }
        public string PlayerBName { get; set; }

        public EPlayerType PlayerAType { get; set; }
        public EPlayerType PlayerBType { get; set; }

        public EShipsCanTouch ShipsCanTouch { get; set; }
        public ENextMoveAfterHit NextMoveAfterHit { get; set; }

        public Dictionary<Ship, int> GameSessionShips { get; set; } = DefaultShipsDictionary();

        public GameSessionParams()
        {
            GameName = "Battleships";
            GameOptionName = "Default Option";
            BoardHeight = 10;
            BoardWidth = 10;
            PlayerAName = "A";
            PlayerBName = "B";
            PlayerAType = EPlayerType.Human;
            PlayerBType = EPlayerType.Human;
            ShipsCanTouch = EShipsCanTouch.No;
            NextMoveAfterHit = ENextMoveAfterHit.OtherPlayer;
        }
        
        private static Dictionary<Ship, int> DefaultShipsDictionary()
        {
            var result = new Dictionary<Ship, int>();
            foreach (var ship in DefaultShips)
            {
                result[ship] = 1;
            }

            return result;
        }
        
    }
}