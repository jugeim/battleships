### Battleships application


* [x] Basic GameBrain (2 boards for 2 players, can place bombs and ships)

* [x] Support saving and loading games:

  * [x] Using text files (JSON format)

  * [x] Using MS Server (Do not have running server so please use your own connection string (in WebApplication/appsettings.json). 
        Commands to create new migration: 

          ```
          dotnet ef migrations --project DAL --startup-project WebApplication add [MigrationName]
          dotnet ef database --project DAL --startup-project WebApplication update
          ```
    )

* [x] Game Console UI:
     * Can draw 2 boards at once, if they both can fit on console, either in one row or one under another. If not, program asks to use separated view
     * Can draw one board with up to 1000 cells width. Show only part of it, navigate using Arrows, exit with Backspace or Enter
     * Using one-board-view on opponent's board pressing Enter will allow to place bomb
     * Can Change Menu UI (Monochrome / Colored)
     * Can Change Game UI (Monochrome / Colored)
     
* [x] Game Web UI:

     - Based on Razor pages
  
     - Default location: localhost:5001 



##### Notes:

- Game can not be started in web and continued in console until all ships are placed. 
- Console game can be saved only after all ships placement.