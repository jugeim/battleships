﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace DAL
{
    public class BattleshipsDbContextFactory : IDesignTimeDbContextFactory<BattleshipsDbContext>
    {
        public BattleshipsDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<BattleshipsDbContext>();
            optionsBuilder.UseSqlServer(
                @"
                    Server=barrel.itcollege.ee,1533;
                    User Id=student;
                    Password=Student.Bad.password.0;
                    Database=jugeim_battleships;
                    MultipleActiveResultSets=true;
                    "
            );

            return new BattleshipsDbContext(optionsBuilder.Options);
        }
    }
}