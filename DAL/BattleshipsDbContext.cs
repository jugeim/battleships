﻿using System;
using System.Linq;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class BattleshipsDbContext : DbContext
    {

        public DbSet<Game> Games { get; set; } = null!;
        public DbSet<GameOption> GameOptions { get; set; } = null!;
        public DbSet<Player> Players { get; set; } = null!;
        public DbSet<Ship> Ships { get; set; } = null!;
        public DbSet<GameOptionShip> GameOptionShips { get; set; } = null!;
        public DbSet<ShipOnBoard> ShipsOnBoard { get; set; } = null!;
        public DbSet<Bomb> Bombs { get; set; } = null!;
        public DbSet<Board> Boards { get; set; } = null!;
        
        public BattleshipsDbContext(DbContextOptions<BattleshipsDbContext> options)
            : base(options)
        {
        }

        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder
                .Entity<Player>()
                .HasOne<Game>()
                .WithOne(x => x.PlayerA)
                .HasForeignKey<Game>(x => x.PlayerAId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder
                .Entity<Player>()
                .HasOne<Game>()
                .WithOne(x => x.PlayerB)
                .HasForeignKey<Game>(x => x.PlayerBId)
                .OnDelete(DeleteBehavior.Restrict);

            /*
            modelBuilder
                .Entity<GameOption>()
                .HasIndex(gameOpt => new
                {
                    gameOpt.NextMoveAfterHit, 
                    gameOpt.ShipsCanTouch,
                    gameOpt.BoardHeight,
                    gameOpt.BoardWidth,
                });
            */

            modelBuilder
                .Entity<GameOptionShip>()
                .HasIndex(optShip => new
                {
                    optShip.ShipId,
                    optShip.Amount
                });

            modelBuilder
                .Entity<Ship>()
                .HasIndex(ship => ship.Name).IsUnique();

            modelBuilder
                .Entity<Game>()
                .HasIndex(game => game.GameOptionId);
            
            
        }
    }
}