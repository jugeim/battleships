﻿using Domain.Enums;
using GameBrain;

namespace ConsoleApp
{
    public class GameParamsDefaultValues
    {
        public string GameName = "Battleships";
        public string PlayerAName = "PlayerA";
        public string PlayerBName = "PlayerB";
        public GameSessionParams GameParams = new GameSessionParams();

        public string PlayerALabel = "First Player Name";
        public string PlayerBLabel = "Second Player Name";
        
        public static GameParamsDefaultValues GetGameDefaultParamValuesForGameType(int gameType)
        {
            var initValues = new GameParamsDefaultValues();
            if (gameType > 1)
            {
                initValues.PlayerBName = "AI Player";
                initValues.PlayerBLabel = "What name It will choose?";
                initValues.GameParams.PlayerBType = EPlayerType.AI;
            }

            if (gameType > 2)
            {
                initValues.PlayerAName = "AI 01";
                initValues.PlayerALabel = "Name for First Ai";
                initValues.PlayerBName = "AI 10";
                initValues.PlayerBLabel = "Name for Second Ai";
                initValues.GameParams.PlayerAType = EPlayerType.AI;
            }

            return initValues;
        }
    }
}