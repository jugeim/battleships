﻿using System.Collections.Generic;
using System.Linq;
using DAL;
using Domain;
using Domain.Enums;
using GameBrain;
using MenuSystem;
using MenuSystem.MenuItems;
using MenuSystem.View;
using Microsoft.EntityFrameworkCore;
using static ConsoleApp.CommonFunctions;
using Ship = GameBrain.Ship;

namespace ConsoleApp
{
    public static class GameCustomizationMenuFunctions
    {
        private static readonly DbContextOptions<BattleshipsDbContext> DbOptions = DbDataManager.DbOptions;
        
        public static ExecutionResults LoadGameOptions(GameSessionParams sessionParams, MenuView menuView)
        {
            using var dbCtx = new BattleshipsDbContext(DbOptions);
            dbCtx.Database.Migrate();

            var availableOptions = dbCtx.GameOptions
                .Include(optionTable => optionTable.GameOptionShips)
                .ThenInclude(optShipsTable => optShipsTable.Ship)
                .ToList();

            var gameOptionsMenu = new Menu(MenuLevel.Level2Plus, "Choose Game Options", menuView);
            gameOptionsMenu.RemoveDefaultMenuItems();
            
            int counter = 1;
            foreach (var dbOptions in availableOptions)
            {
                gameOptionsMenu.AddMenuItem(new MenuItem(counter.ToString(), dbOptions.Name, 
                    () => ShowGameOption(sessionParams, dbOptions.GameOptionId, menuView)));
                counter++;
            }

            string label = "Default Option";
            if (availableOptions.Count > 0)
            {
                gameOptionsMenu.AddMenuItem(new MenuItemSeparator("-"));
                label = "Show Current Option";
            }
            gameOptionsMenu.AddMenuItem(new MenuItem(counter.ToString(), label,
                () => ShowGameOption(sessionParams, menuView)));
            counter++;
            
            gameOptionsMenu.AddMenuItem(new MenuItem(counter.ToString(), "Make Custom Option", 
                () => CreateGameOption(sessionParams, availableOptions, menuView)));
            
            gameOptionsMenu.AddMenuItem(new MenuItemEscape(NavigationCommand.ToPrevious));

            return gameOptionsMenu.RunMenu();
        }

        private static ExecutionResults ShowGameOption(GameSessionParams sessionParams, MenuView menuView)
        {
            var showOptionMenu = new Menu(MenuLevel.Level2Plus, sessionParams.GameOptionName, menuView);
            showOptionMenu.RemoveDefaultMenuItems();
            showOptionMenu.AddMenuItem(new MenuItem("1", $"Board Height: {sessionParams.BoardHeight}", 
                () => new ExecutionResults()));
            showOptionMenu.AddMenuItem(new MenuItem("2", $"Board Width: {sessionParams.BoardWidth}", 
                () => new ExecutionResults()));
            showOptionMenu.AddMenuItem(new MenuItem("3", $"Ship Placing Rule (can touch): {sessionParams.ShipsCanTouch}", 
                () => new ExecutionResults()));
            showOptionMenu.AddMenuItem(new MenuItem("4", $"Next Turn After Hit: {sessionParams.NextMoveAfterHit}", 
                () => new ExecutionResults()));
            showOptionMenu.AddMenuItem(new MenuItem("5", "Option Ships", 
                () => ShowShips(sessionParams.GameSessionShips, menuView, false)));
            showOptionMenu.AddMenuItem(new MenuItemSeparator("-"));
            showOptionMenu.AddMenuItem(new MenuItem("c", "Choose Option", 
                () => new ExecutionResults().WithCommand(NavigationCommand.ToPrevious)
            ));
            return showOptionMenu.RunMenu();
        }

        private static ExecutionResults ShowGameOption(GameSessionParams sessionParams, int optionId, MenuView menuView)
        {
            using var dbCtx = new BattleshipsDbContext(DbOptions);
            dbCtx.Database.Migrate();
            
            var option = dbCtx.GameOptions
                .Include(optionTable => optionTable.GameOptionShips)
                .ThenInclude(optShipsTable => optShipsTable.Ship)
                .First(x => x.GameOptionId == optionId);

            var optionShips = option.GameOptionShips.ToDictionary(
                optShip => new Ship(optShip.Ship), 
                optShip => optShip.Amount);
            
            var showOptionMenu = new Menu(MenuLevel.Level2Plus, option.Name, menuView);
            showOptionMenu.RemoveDefaultMenuItems();
            showOptionMenu.AddMenuItem(new MenuItem("1", $"Board Height: {option.BoardHeight}", 
                () => new ExecutionResults()));
            showOptionMenu.AddMenuItem(new MenuItem("2", $"Board Width: {option.BoardWidth}", 
                () => new ExecutionResults()));
            showOptionMenu.AddMenuItem(new MenuItem("3", $"Ship Placing Rule (can touch): {option.ShipsCanTouch}", 
                () => new ExecutionResults()));
            showOptionMenu.AddMenuItem(new MenuItem("4", $"Next Turn After Hit: {option.NextMoveAfterHit}", 
                () => new ExecutionResults()));
            showOptionMenu.AddMenuItem(new MenuItem("5", "Option Ships", 
                () => ShowShips(optionShips, menuView, false)));
            showOptionMenu.AddMenuItem(new MenuItemSeparator("-"));
            showOptionMenu.AddMenuItem(new MenuItemEscape(NavigationCommand.ToPrevious));
            showOptionMenu.AddMenuItem(new MenuItem("c", "Choose Option", 
                () => ApplyOptionToGameSession(sessionParams, option)
                ));
            return showOptionMenu.RunMenu();
        }

        private static ExecutionResults ApplyOptionToGameSession(GameSessionParams sessionParams, GameOption option)
        {
            sessionParams.GameOptionId = option.GameOptionId;
            sessionParams.GameOptionName = option.Name;
            sessionParams.BoardHeight = option.BoardHeight;
            sessionParams.BoardWidth = option.BoardWidth;
            sessionParams.ShipsCanTouch = option.ShipsCanTouch;
            sessionParams.NextMoveAfterHit = option.NextMoveAfterHit;
            sessionParams.GameSessionShips = option.GameOptionShips
                .ToDictionary(optShip => new Ship(optShip.Ship),
                    optShip => optShip.Amount);
            
            return new ExecutionResults().WithCommand(NavigationCommand.ToMain);
        }
        
        private static ExecutionResults CreateGameOption(GameSessionParams sessionParams, 
            List<GameOption> existingOptions, MenuView menuView)
        {
            var sameNameCount = existingOptions.FindAll(x => x.Name.Contains("Custom Option")).Count;
            var optionName = $"Custom Option {(sameNameCount > 0 ? sameNameCount.ToString() : "")}";
            var boardHeight = 10;
            var boardWidth = 10;
            var shipsCanTouch = EShipsCanTouch.Corner;
            var nextMoveAfterHit = ENextMoveAfterHit.OtherPlayer;
            var gameOptionShips = sessionParams.GameSessionShips;

            var customOptionMenu = new Menu(MenuLevel.Level2Plus, "Make custom Option", menuView);
            
            customOptionMenu.RemoveDefaultMenuItems();
            customOptionMenu.AddMenuItem(new MenuItemReflector("1", "Option Name", optionName, 
                () =>
                {
                    var results = UpdateStringValue(out optionName, optionName, "option name", 3, 21);
                    var name = optionName;
                    sameNameCount = existingOptions.FindAll(x => x.Name.Contains(name)).Count;
                    optionName = $"{optionName}{(sameNameCount > 0 ? ' ' + sameNameCount.ToString() : "")}";
                    return results;
                }));
            customOptionMenu.AddMenuItem(new MenuItemReflector("2", "Board Height", boardHeight.ToString(), 
                () => UpdateIntValue(out boardHeight, boardHeight, "Board Height", 10, 1000)));
            customOptionMenu.AddMenuItem(new MenuItemReflector("3", "Board Width", boardWidth.ToString(), 
                () => UpdateIntValue(out boardWidth, boardWidth, "Board Width", 5, 1000)));
            
            customOptionMenu.AddMenuItem(new MenuItemReflector("4", "Can Ships Touch", shipsCanTouch.ToString(), 
                () => UpdateEnumValue(out shipsCanTouch, shipsCanTouch, "ShipsCanTouch")));
            customOptionMenu.AddMenuItem(new MenuItemReflector("5", "Next Move After Hit", nextMoveAfterHit.ToString(), 
                () => UpdateEnumValue(out nextMoveAfterHit, nextMoveAfterHit, "NextMoveAfterHit")));
            
            customOptionMenu.AddMenuItem(new MenuItem("6", "Game Option Ships", 
                () => ShowShips(gameOptionShips, menuView, true)));
            
            customOptionMenu.AddMenuItem(new MenuItemSeparator("---", 30));
            customOptionMenu.AddMenuItem(new MenuItemEscape("c", "cancel", NavigationCommand.ToPrevious));
            customOptionMenu.AddMenuItem(new MenuItem("s", "Save Game Option", () =>
            {
                
                sessionParams.BoardHeight = boardHeight;
                sessionParams.BoardWidth = boardWidth;
                sessionParams.GameOptionName = optionName;
                sessionParams.GameSessionShips = gameOptionShips;
                sessionParams.ShipsCanTouch = shipsCanTouch;
                sessionParams.NextMoveAfterHit = nextMoveAfterHit;

                var gameOptionId = DbDataManager.SaveGameOptionIntoDatabase(sessionParams);
                
                sessionParams.GameOptionId = gameOptionId;

                return new ExecutionResults().WithCommand(NavigationCommand.ToMain);
            }));
            return customOptionMenu.RunMenu();
        }

        private static ExecutionResults ShowShips(Dictionary<Ship, int> optionShips, MenuView menuView, bool shouldBeInteractive)
        {
            
            var gameOptionShipsMenu = new Menu(MenuLevel.Level2Plus, "Game Option Ships", menuView);
            gameOptionShipsMenu.RemoveDefaultMenuItems();
            
            int counter = 1;
            foreach (var (optShip, amount) in optionShips)
            {
                gameOptionShipsMenu.AddMenuItem(
                    shouldBeInteractive 
                        ? CreateMenuItemForShip(optionShips, optShip, amount, counter, gameOptionShipsMenu)
                        : new MenuItem(counter.ToString(), $"{optShip}: {amount}", () => new ExecutionResults()));
                counter++;
            }

            if (shouldBeInteractive)
            {
                gameOptionShipsMenu.AddMenuItem(new MenuItem("Db", "Choose Ship from Db", 
                    () => LoadShipsFormDatabase(optionShips, gameOptionShipsMenu)));
            }
            gameOptionShipsMenu.AddMenuItem(new MenuItemEscape(NavigationCommand.ToPrevious));

            return gameOptionShipsMenu.RunMenu();
        }


        private static ExecutionResults LoadShipsFormDatabase(Dictionary<Ship, int> optionShips, Menu parentMenu)
        {
            using var dbCtx = new BattleshipsDbContext(DbOptions);
            dbCtx.Database.Migrate();

            var availableShips = dbCtx.Ships
                .ToList();

            var dbShipsMenu = new Menu(MenuLevel.Level2Plus, "Choose Ship", parentMenu.View);
            dbShipsMenu.RemoveDefaultMenuItems();
            
            int counter = 1;
            foreach (var dbShip in availableShips)
            {
                dbShipsMenu.AddMenuItem(CreateMenuItemForDbShip(optionShips, dbShip, counter, parentMenu));
                counter++;
            }

            dbShipsMenu.AddMenuItem(new MenuItem("N", "Make New Ship", 
                () => CreateShip(optionShips, dbShipsMenu, parentMenu, dbCtx)));
            
            dbShipsMenu.AddMenuItem(new MenuItemSeparator("-"));
            dbShipsMenu.AddMenuItem(new MenuItemEscape(NavigationCommand.ToPrevious));
            dbShipsMenu.AddMenuItem(new MenuItemEscape("m", "Return to Game Creation Menu", NavigationCommand.ToMain));

            return dbShipsMenu.RunMenu();
        }

        private static ExecutionResults CreateShip(Dictionary<Ship, int> optionShips, Menu dbShipsMenu, Menu gameOptShipsMenu
            , BattleshipsDbContext dbCtx)
        {
            dbCtx.Database.Migrate();

            var existingShips = dbCtx.Ships
                .ToList();

            var occupiedNames = existingShips.Select(ship => ship.Name).ToArray();
            
            var name = "New Ship";
            var size = 2;
            
            var shipCreationMenu = new Menu(MenuLevel.Level2Plus, "New Ship", dbShipsMenu.View);
            shipCreationMenu.RemoveDefaultMenuItems();
            
            shipCreationMenu.AddMenuItem(new MenuItemReflector("1", "Ship Name", name, 
                () => UpdateUniqueStringValue(out name, name, "Ship name",
                    1, 20, occupiedNames)));
            shipCreationMenu.AddMenuItem(new MenuItemReflector("2", "Ship Size", size.ToString(), 
                () => UpdateIntValue(out size, size, "Ship size", 1, 100)));
            
            dbShipsMenu.AddMenuItem(new MenuItemSeparator("-"));
            dbShipsMenu.AddMenuItem(new MenuItemEscape("c", "Cancel", NavigationCommand.ToPrevious));
            shipCreationMenu.AddMenuItem(new MenuItem("s", "Save Ship", () =>
            {
                var dbShip = new Domain.Ship {Name = name, Size = size};
                dbCtx.Ships.Add(dbShip);
                dbCtx.SaveChanges();
                dbShipsMenu.InsertMenuItemBeforeGivenUserChoice(
                    CreateMenuItemForDbShip(optionShips, dbShip, dbCtx.Ships.Count(), gameOptShipsMenu) , "n");
                return new ExecutionResults().WithCommand(NavigationCommand.ToPrevious);
            }));

            return shipCreationMenu.RunMenu();
        }

        private static MenuItem CreateMenuItemForShip(Dictionary<Ship, int> optionShips, Ship optShip, 
            int amount, int counter, Menu gameOptionShipsMenu)
        {
            return new MenuItemReflector(counter.ToString(), optShip.Name,
                amount.ToString(),
                () =>
                {
                    var shipMenu = new Menu(MenuLevel.Level2Plus, optShip.Name, gameOptionShipsMenu.View);
                    shipMenu.RemoveDefaultMenuItems();

                    shipMenu.AddMenuItem(new MenuItem("i", "Increase amount",
                        () =>
                        {
                            var newAmount = ++optionShips[optShip];
                            return new ExecutionResults
                            {
                                StrValue = newAmount.ToString(),
                                Command = NavigationCommand.ToPrevious
                            };
                        }));

                    shipMenu.AddMenuItem(new MenuItem("d", "Decrease amount",
                        () =>
                        {
                            var newAmount = --optionShips[optShip];
                            if (newAmount == 0)
                            {
                                optionShips.Remove(optShip);
                                var index = gameOptionShipsMenu.MenuItems.FindIndex(
                                    item => item.Label.Contains(optShip.Name) 
                                            && ((MenuItemReflector) item).Value.Trim() == "1");
                                gameOptionShipsMenu.MenuItems.RemoveAt(index);
                            }
                            return new ExecutionResults
                            {
                                StrValue = newAmount.ToString(),
                                Command = NavigationCommand.ToPrevious
                            };
                        }));

                    return shipMenu.RunMenu();
                });
        }

        private static MenuItem CreateMenuItemForDbShip(Dictionary<Ship, int> optionShips, Domain.Ship dbShip, int counter, Menu parentMenu)
        {
            return new MenuItem(counter.ToString(), dbShip.ToString(),
                () =>
                {
                    var gameShip = new Ship(dbShip);
                    if (optionShips.ContainsKey(gameShip))
                    {
                        var newAmount = ++optionShips[gameShip];
                        MenuItemReflector shipItem = (MenuItemReflector) parentMenu.MenuItems
                            .Find(item => item.Label.Contains(gameShip.Name))!;
                        shipItem.Value = newAmount.ToString();
                    }
                    else
                    {
                        optionShips[gameShip] = 1;
                        parentMenu.InsertMenuItemBeforeGivenUserChoice(
                            CreateMenuItemForShip(optionShips, gameShip, 1, optionShips.Count, parentMenu),
                            "db");
                    }

                    return new ExecutionResults().WithCommand(NavigationCommand.ToPrevious);
                });
        }
 
    }
}