﻿using System;
using System.IO;
using System.Linq;
using GameBrain;
using MenuSystem;
using MenuSystem.MenuItems;
using MenuSystem.View;

namespace ConsoleApp
{
    public static class FileDataManager
    {
        private const string PathToGameSaves = "../../../../GameSaves"; // navigate to solution folder and enter GameSaves folder 
        
        public static ExecutionResults SaveIntoJsonAction(Battleships game)
        {
            var defaultName = "save_" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + ".json";
            Console.Write($"File name ({defaultName}):");
            var fileName = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(fileName))
            {
                fileName = defaultName;
            } else if (!fileName.EndsWith(".json"))
            {
                fileName += ".json";
            }
            
            var path = Path.Combine(PathToGameSaves, fileName);

            
            var serializedGame = game.GetSerializedGameState();

            File.WriteAllText(path, serializedGame);

            return new ExecutionResults { Command = NavigationCommand.Stay };
        }
        
        public static ExecutionResults LoadFromJsonAction(Battleships game, MenuView menuView)
        {
            var files = Directory.EnumerateFiles(PathToGameSaves, "*.json").ToList();

            var loadMenu = new Menu(MenuLevel.Level2Plus, "Load Game", menuView);
            for (int i = 0; i < files.Count; i++)
            {
                loadMenu.AddMenuItem(new MenuItemFileLoader(i.ToString(), files[i], game));
            }

            return loadMenu.RunMenu();
        }
    }
}