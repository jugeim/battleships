﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using AI;
using ConsoleApp.Exceptions;
using Domain.Enums;
using GameBrain;
using GameBrain.Exceptions;
using GameConsoleUI;
using MenuSystem;
using MenuSystem.MenuItems;
using MenuSystem.View;
using static ConsoleApp.CommonFunctions;
using static ConsoleApp.GameParamsDefaultValues;
using static ConsoleApp.ShipPlacementFunctions;

namespace ConsoleApp
{
    class Program
    {

        public static MenuView MenuView = new MenuView();
        public static BattleshipBoardDrawer GameView = new MonochromeBoardDrawer();  
        
        static void Main(string[] args)
        {
            //DbDataManager.LoadDefaultValues(true); // can start and work with (almost) empty db 

            var mainMenu = new Menu(MenuLevel.Level0, MenuView);
            mainMenu.AddMenuItem(new MenuItem("1", "New game human vs human", () => GameInitialization(1)));
            mainMenu.AddMenuItem(new MenuItem("2", "New game human vs AI", () => GameInitialization(2)));
            mainMenu.AddMenuItem(new MenuItem("3", "New game AI vs AI",  () => GameInitialization(3)));
            mainMenu.AddMenuItem(new MenuItemSeparator("-", 28));

            mainMenu.AddMenuItem(new MenuItem("L1", "Load Game File", () =>
            {
                var game = new Battleships(new GameSessionParams());
                try
                {
                    var loadMenuExecResults = FileDataManager.LoadFromJsonAction(game, MenuView);
                    return loadMenuExecResults.ShouldLoadGame ? GameProcess(game) : loadMenuExecResults;
                }
                catch (Exception)
                {
                    ConsoleErrorMessage("Selected save file is broken. Could not load it.");
                    return new ExecutionResults();
                }
                
            }));
            mainMenu.AddMenuItem(new MenuItem("L2", "Load Game From Database", () =>
            {
                var game = new Battleships(new GameSessionParams());
                var loadMenuExecResults = DbDataManager.LoadFromDatabaseAction(game, MenuView);
                return loadMenuExecResults.ShouldLoadGame ? GameProcess(game) : loadMenuExecResults;
            }));

            var settingsMenu = new Menu(MenuLevel.Level1, "Game Settings");
            mainMenu.AddMenuItem(new MenuItem("S", "Game Settings", settingsMenu.RunMenu));
            settingsMenu.AddMenuItem(new MenuItemReflector("1", "Game View", "Monochrome",SwitchGameView));
            settingsMenu.AddMenuItem(new MenuItemReflector("2", "Menu View", "Monochrome", () =>
                {
                    var menus = new[] { mainMenu, settingsMenu };
                    return SwitchMenuView(menus);
                }));

            mainMenu.RunMenu();
        }

        private static ExecutionResults SwitchMenuView(IEnumerable<Menu> menus)
        {
            var results = new ExecutionResults {Command = NavigationCommand.Stay };

            if (MenuView.GetType() == typeof(ColoredMenuView))
            {
                MenuView = new MenuView();
                results.StrValue = "Monochrome";
            }
            else
            {
                MenuView = new ColoredMenuView();
                results.StrValue = "Colored";
            }
            
            foreach (var menu in menus)
            {
                menu.View = MenuView;
            }
            return results;
        }

        private static ExecutionResults SwitchGameView()
        {
            var results = new ExecutionResults {Command = NavigationCommand.Stay };
            
            if (GameView.GetType() == typeof(MonochromeBoardDrawer))
            {
                GameView = new ColoredBoardDrawer();
                results.StrValue = "Colored";
            }
            else
            {
                GameView = new MonochromeBoardDrawer();
                results.StrValue = "Monochrome";
            }
            return results;
        }

        /**
         * 1 — PvP
         * 2 — PvAI
         * 3 — AIvAI
         */
        private static ExecutionResults GameInitialization(int gameType)
        {
            var initValues = GetGameDefaultParamValuesForGameType(gameType);
            var gameName = initValues.GameName;
            var playerAName = initValues.PlayerAName;
            var playerBName = initValues.PlayerBName;
            var gameParams = initValues.GameParams;
            
            var gameInitMenu = new Menu(MenuLevel.Level0, "Battleships", MenuView);
            gameInitMenu.RemoveDefaultMenuItems();
            gameInitMenu.AddMenuItem(new MenuItemEscape("s", "Start Game", NavigationCommand.Exit));
            gameInitMenu.AddMenuItem(new MenuItemSeparator("--- Customization Settings ", 30));
            gameInitMenu.AddMenuItem(new MenuItemReflector("1", "Game Name", gameName, 
                () => UpdateStringValue(out gameName, gameName, "game name", 1, 21)));
            gameInitMenu.AddMenuItem(new MenuItemReflector("2", initValues.PlayerALabel, playerAName, 
                () => UpdateStringValue(out playerAName, playerAName, "1st player name", 1, 21)));
            gameInitMenu.AddMenuItem(new MenuItemReflector("3", initValues.PlayerBLabel, playerBName, 
                () => gameType == 2 
                    ? GenerateRandomStringValue(out playerBName, 1, 21) 
                    : UpdateStringValue(out playerBName, playerBName, "2nd player name", 1, 21)));
            
            gameInitMenu. AddMenuItem(new MenuItem("o", "Game Options", 
                () => GameCustomizationMenuFunctions.LoadGameOptions(gameParams, MenuView)));
            
            gameInitMenu.RunMenu();

            gameParams.GameName = gameName;
            gameParams.PlayerAName = playerAName;
            gameParams.PlayerBName = playerBName;
            
            var game = new Battleships(gameParams);

            return GamePreparationProcess(game);
        }

        private static ExecutionResults GamePreparationProcess(Battleships game)
        {
            var shipsForPlayerA = new Dictionary<Ship, int>(game.GameSessionShips);
            var shipsForPlayerB = new Dictionary<Ship, int>(game.GameSessionShips);

            var shipsForPlayers = new Dictionary<bool, Dictionary<Ship, int>>
            {
                [true] = shipsForPlayerA,
                [false] = shipsForPlayerB
            };

            var gamePrepMenu = new Menu(MenuLevel.Level0, $"Preparation Menu for {game.PlayerA.Name}", MenuView);
            gamePrepMenu.RemoveDefaultMenuItems();
            gamePrepMenu.AddMenuItem(new MenuItem("1", "Choose ship and place",
                () => ChooseShipAndPlace(game, shipsForPlayers, false, MenuView, GameView)));
            gamePrepMenu.AddMenuItem(new MenuItem("2", "Place all ships",
                () => PlaceShips(game, shipsForPlayers, GameView)));
            gamePrepMenu.AddMenuItem(new MenuItem("3", "Choose ship and place randomly",
                () => ChooseShipAndPlace(game, shipsForPlayers, true, MenuView, GameView)));
            gamePrepMenu.AddMenuItem(new MenuItem("4", "Place all ships randomly",
                () => PlaceShipsRandomly(game, shipsForPlayers)));
            gamePrepMenu.AddMenuItem(new MenuItem("5", "Undo last ship placement",
                () => UndoLastShipPlacement(game, shipsForPlayers)));
            gamePrepMenu.AddMenuItem(new MenuItem("6", "Show Board",
                () => ShowBoard(game, false, gamePrepMenu)));
            gamePrepMenu.AddMenuItem(new MenuItemReflector("r", "I'm Ready!", "",
                () => FinalizePreparationForPlayer(game, gamePrepMenu, shipsForPlayers)));
            
            if (game.PlayerA.PlayerType == EPlayerType.AI)
            {
                PlaceShipsRandomly(game, shipsForPlayers);
                FinalizePreparationForPlayer(game, gamePrepMenu, shipsForPlayers);
            }
            else
            {
                gamePrepMenu.RunMenu();
            }

            return GameProcess(game);
        }

        private static ExecutionResults FinalizePreparationForPlayer(Battleships game, Menu menu, 
            Dictionary<bool, Dictionary<Ship, int>> availableShips)
        {
            var currentPlayerShips = availableShips[game.NextMoveByA];
            var hasDoneWithShips = AreAllShipsPlaced(currentPlayerShips);
            
            if (!hasDoneWithShips) 
                return new ExecutionResults {StrValue = "(can't be ready before all ships were placed)"};

            game.SwitchPlayer();
            
            if (!game.NextMoveByA && game.PlayerB.PlayerType == EPlayerType.AI)
            {
                PlaceShipsRandomly(game, availableShips);
                game.SwitchPlayer();
            }
            else
            {
                ClearAndWaitForKey();
            }
            
            if (game.NextMoveByA)
            {
                return new ExecutionResults().WithCommand(NavigationCommand.Exit);
            }
            
            menu.Title = $"Preparation Menu for {game.PlayerB.Name}";
            return new ExecutionResults();
        }

        private static bool AreAllShipsPlaced(Dictionary<Ship, int> playerAvailableShips)
        {
            foreach (var (_, count) in playerAvailableShips)
            {
                if (count > 0) return false;
            }
            return true;
        }
        
        private static ExecutionResults GameProcess(Battleships game)
        {
            var menu = new Menu(MenuLevel.Level1, $"{(game.NextMoveByA ? game.PlayerA.Name : game.PlayerB.Name)}", MenuView);
            menu.ModifyMenuItem("x", "x", "Exit Program");
            menu.ModifyMenuItem("m", "e", "Exit Current Game");
            menu.AddMenuItem(new MenuItem("m", "Make a move", () => MakeAMove(game, menu)));
            menu.AddMenuItem(new MenuItem("1", "Show my board", 
                () => ShowBoard(game, false, menu)));
            menu.AddMenuItem(new MenuItem("2", "Show opponent's board", 
                () => ShowBoard(game, true, menu)));
            menu.AddMenuItem(new MenuItem("s", "Save game", 
                () => FileDataManager.SaveIntoJsonAction(game))
            );
            menu.AddMenuItem(new MenuItem("db", "Save to Db", 
                () => DbDataManager.SaveIntoDatabaseAction(game)));
            menu.AddMenuItem(new MenuItem( "l1", "Load game file",
                () => FileDataManager.LoadFromJsonAction(game, MenuView))
            );
            menu.AddMenuItem(new MenuItem( "l2", "Load game from db",
                () => DbDataManager.LoadFromDatabaseAction(game, MenuView))
            );
            
            var userChoice = menu.RunMenu();

            return userChoice;
        }

        private static void AiMakeAMove(Battleships game, Menu menu, bool shouldSwitch)
        {
            Thread messageThread = new Thread(() =>
            {
                Console.Clear();
                Console.WriteLine("Ai is making its turn(s)");
                Thread.Sleep(500);
                Console.Write(".");
                Thread.Sleep(200);
                Console.Write(".");
                Thread.Sleep(200);
                Console.Write(".");
                Thread.Sleep(200);

            });
            messageThread.Start();
            var isAiTurn = true;
            if (shouldSwitch) game.SwitchPlayer();
            
            var ai = game.GetCurrentPlayer().GetPlayerAi();
            if (!ai.IsGameKnowledgeSet)
            {
                ai.UpdateAiKnowledge(game.GetPublicSessionParametersForAiPlayer());
            } 
            
            while (isAiTurn)
            {
                var isHit = PlaceBombOnBoard(game);
                ai.ProcessMoveResults(isHit);

                if (!isHit || game.NextMoveAfterHit != ENextMoveAfterHit.SamePlayer) isAiTurn = false;
            }
            
            if (shouldSwitch) game.SwitchPlayer();
            else GiveTurnToSecondPlayer(game, menu);

            messageThread.Join();
        }

        private static ExecutionResults MakeAMove(Battleships game, Menu menu)
        {
            Console.Clear();
            GameView.Reset();
            var hasDrawn = GameView.DrawTwoBoards(game);
            if (!hasDrawn)
            {
                Console.WriteLine("Press key to continue");
                Console.ReadKey();
                return new ExecutionResults();
            }

            if (game.GetCurrentPlayer().PlayerType == EPlayerType.AI)
            {
                AiMakeAMove(game, menu, false);
            }
            else
            {
                bool isHit = false;
                var wasBombingAborted = false;
                try
                {
                    isHit = PlaceBombOnBoard(game); // true if bomb hit ship
                }
                catch (AbortException)
                {
                    wasBombingAborted = true;
                }
           
                Console.Clear();
                if (!wasBombingAborted)
                {
                    GameView.DrawTwoBoards(game);
                    if (game.NextMoveAfterHit == ENextMoveAfterHit.SamePlayer && isHit) KeepTurnAfterHit();
                    else
                    {
                        if (game.PlayerB.PlayerType == EPlayerType.AI) 
                            AiMakeAMove(game, menu, true);
                        else 
                            GiveTurnToSecondPlayer(game, menu);
                    }
                }
            }
            
            return new ExecutionResults();
        }

        private static ExecutionResults ShowBoard(Battleships game, bool isOpponentsBoard, Menu menu)
        {
            var isATurn = game.NextMoveByA;
            
            var board = isATurn && isOpponentsBoard || !isATurn && !isOpponentsBoard ? game.PlayerB.Board : game.PlayerA.Board;
            
            Console.Clear();
            var title = isOpponentsBoard ? "Opponent's board" : "My board"; 
            GameView.Reset();

            var shouldExit = NavigateBoard(board, title, isOpponentsBoard);

            if (!isOpponentsBoard || shouldExit) return new ExecutionResults();
            
            if (game.GetCurrentPlayer().PlayerType == EPlayerType.AI)
            {
                AiMakeAMove(game, menu, false);
            }
            else
            {
                bool isHit = false;
                var wasBombingAborted = false;
                try
                {
                    isHit = PlaceBombOnBoard(game); // true if bomb hit ship
                }
                catch (AbortException)
                {
                    wasBombingAborted = true;
                }

                Console.Clear();
                if (!wasBombingAborted)
                {
                    GameView.DrawBoard(board, title, isOpponentsBoard);
                    if (game.NextMoveAfterHit == ENextMoveAfterHit.SamePlayer && isHit) KeepTurnAfterHit();
                    else
                    {
                        if (game.PlayerB.PlayerType == EPlayerType.AI)
                        {
                            AiMakeAMove(game, menu, true);
                        }
                        else
                        {
                            GiveTurnToSecondPlayer(game, menu);
                        }
                    }
                }
            }

            return new ExecutionResults();
        }

        private static bool NavigateBoard(Board board, string title, bool isOpponentsBoard)
        {
            var listener = new ArrowListener(false);
            ConsoleKey commandKey;
            do
            {
                GameView.DrawBoard(board, title, isOpponentsBoard);
                var newCommandKey = listener.Listen();
                if (newCommandKey == ConsoleKey.LeftArrow)
                    GameView.MoveLeft();
                else if (newCommandKey == ConsoleKey.RightArrow)
                    GameView.MoveRight(board.Columns);
                else if (newCommandKey == ConsoleKey.UpArrow)
                    GameView.MoveUp();
                else if (newCommandKey == ConsoleKey.DownArrow) GameView.MoveDown(board.Rows);

                commandKey = newCommandKey;
                Console.Clear();
                
            } while (!(commandKey == ConsoleKey.Enter || commandKey == ConsoleKey.Backspace));
            GameView.DrawBoard(board, title, isOpponentsBoard);
            return commandKey == ConsoleKey.Backspace; // if true, then should not ask user input
        }
        
        /**
         * true if bomb has hit a ship, false if not
         * throw AbortException if player aborted bomb placing
         * throw GameEndException if game was ended
         */
        private static bool PlaceBombOnBoard(Battleships game)
        {
            var currentPlayer = game.NextMoveByA ? game.PlayerA : game.PlayerB;
            bool isHit = false;
            bool shouldContinue; 
            do
            {
                shouldContinue = false;
               
                var (row, col) = currentPlayer.PlayerType == EPlayerType.AI ? 
                    currentPlayer.GiveMoveCoordinates() 
                    : GetMoveCoordinates();

                if (row == -999 && col == -999) throw new AbortException("abort bomb placing");
                try
                {
                    isHit = game.PlaceBomb(row, col);
                }
                catch (GameEndException ge)
                {
                    Console.WriteLine(ge);
                    throw new Exception("Not Implemented yet");
                }
                catch (ArgumentException e) // attempt to place bomb on already bombed location 
                {
                    ConsoleClearLine();
                    Console.WriteLine(e.Message);
                    shouldContinue = true; // one more bomb placing cycle
                }
                
            } while (shouldContinue);

            return isHit;
        }

        private static (int row, int col) GetMoveCoordinates()
        {
            var row = 0;
            var col = 0;
            Console.Write("Give (row, col):");
            while (true)
            {
                var userInput = Console.ReadLine();
                userInput ??= "";
                
                if (userInput.ToLower().Equals("abort")) return (-999, -999);
                string[] coordinates = Regex.Split(userInput, @"[^\d]+");
                var canParseCoordinates = coordinates.Length == 2 
                                          && int.TryParse(coordinates[0].Trim(), out row) 
                                          && int.TryParse(coordinates[1].Trim(), out col);
                if (canParseCoordinates) break;
                
                ConsoleClearLine();
                Console.Write("Invalid Coordinates, try again (row: int, column: int) or abort ('abort') >");
            }
            return (row, col);
        }

        private static void GiveTurnToSecondPlayer(Battleships game, Menu menu)
        {
            Console.WriteLine("Press any key to give turn to second player");
            Console.ReadKey();
            game.SwitchPlayer();
            menu.Title = $"{(game.NextMoveByA ? game.PlayerA.Name : game.PlayerB.Name)}";
            ClearAndWaitForKey();
        }
        
        private static void KeepTurnAfterHit()
        {
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
        }
    }
}