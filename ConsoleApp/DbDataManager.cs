﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using Domain;
using GameBrain;
using MenuSystem;
using MenuSystem.MenuItems;
using MenuSystem.View;
using Microsoft.EntityFrameworkCore;

namespace ConsoleApp
{
    public static class DbDataManager
    {
        
        private const string ConnectionString = @"
                    Server=barrel.itcollege.ee,1533;
                    User Id=student;
                    Password=Student.Bad.password.0;
                    Database=jugeim_battleships;
                    MultipleActiveResultSets=true;
                    "
            ;
        
        public static readonly DbContextOptions<BattleshipsDbContext> DbOptions = 
            new DbContextOptionsBuilder<BattleshipsDbContext>()
            .UseSqlServer(ConnectionString)
            .Options;

        /**
         * Function used it case db data was totally cleared and need to set up some default game objects/values
         */
        public static void LoadDefaultValues(bool shouldClearDatabase)
        {
            if (shouldClearDatabase) ClearDatabase();

            var defaultShipsIntoDb = GameSessionParams.DefaultShips
                .Select(gameShip => new Domain.Ship
                {
                    Name = gameShip.Name,
                    Size = gameShip.Size
                }).ToList();
            
            using var dbCtx = new BattleshipsDbContext(DbOptions);
            dbCtx.Ships.AddRange(defaultShipsIntoDb);
            dbCtx.SaveChanges();
        }

        public static void ClearDatabase()
        {
            using var dbCtx = new BattleshipsDbContext(DbOptions);
            dbCtx.Database.EnsureDeleted();
            dbCtx.SaveChanges();
            Console.WriteLine("Here");
            dbCtx.Database.Migrate();
            dbCtx.SaveChanges();
        }

        public static ExecutionResults SaveIntoDatabaseAction(Battleships game)
        {
            using var dbCtx = new BattleshipsDbContext(DbOptions);
            //dbCtx.Database.EnsureDeleted();
            dbCtx.Database.Migrate();
            if (game.GameId > 0)
            { 
                UpdateGameRecord(game, dbCtx);
            }
            else
            {
                SaveNewGameIntoDatabase(game, dbCtx);
            }
            return new ExecutionResults();
        }

        public static ExecutionResults LoadFromDatabaseAction(Battleships emptyGame, MenuView menuView)
        {
            using var dbCtx = new BattleshipsDbContext(DbOptions);
            dbCtx.Database.Migrate();

            var availableGames = dbCtx.Games.ToList();

            var loadMenu = new Menu(MenuLevel.Level2Plus, "Load Game from Database", menuView);
            int counter = 1;
            foreach (var dbGame in availableGames)
            {
                loadMenu.AddMenuItem(new MenuItem(counter.ToString(), dbGame.Name, 
                     () => LoadGameSessionFromDatabase(emptyGame, dbGame.GameId)));
                counter++;
            }

            return loadMenu.RunMenu();
        }

        private static ExecutionResults LoadGameSessionFromDatabase(Battleships emptyGame, int gameId)
        {
            using var dbCtx = new BattleshipsDbContext(DbOptions);
            dbCtx.Database.Migrate();
            
            var dbGame  = dbCtx.Games.Find(gameId);
            
            dbCtx.GameOptions.Where(o => o.GameOptionId == dbGame.GameOptionId).Load();
            
            dbCtx.GameOptionShips
                .Where(x => x.GameOptionId == dbGame.GameOptionId).Load();
            var gameOptShips = dbCtx.GameOptionShips
                .Where(x => x.GameOptionId == dbGame.GameOptionId).ToList();
            
            foreach (var s in gameOptShips)
            {
                dbCtx.Ships.Find(s.ShipId);
            }

            dbCtx.Players.Find(dbGame.PlayerAId);
            dbCtx.Players.Find(dbGame.PlayerBId);
            var b1= dbCtx.Boards.Find(dbGame.PlayerA.BoardId);
            var b2 = dbCtx.Boards.Find(dbGame.PlayerB.BoardId);
                
            dbCtx.Bombs.Where(x => x.BoardId == b1.BoardId).Load();
            dbCtx.Bombs.Where(x => x.BoardId == b2.BoardId).Load();
                
            dbCtx.ShipsOnBoard.Where(x => x.BoardId == b1.BoardId).Load();
            dbCtx.ShipsOnBoard.Where(x => x.BoardId == b2.BoardId).Load();

            emptyGame.SetGameStateFromDbData(dbGame);
            
            return new ExecutionResults
            {
                Command = NavigationCommand.ToPrevious,
                ShouldLoadGame = true
            };
        }

        private static void UpdateGameRecord(Battleships battleships, BattleshipsDbContext dbCtx)
        {
            // Load game without options 
            var dbGame = dbCtx.Games.Find(battleships.GameId);
            if (dbGame == null) throw new Exception("No game in db with GameId " + battleships.GameId);

            dbCtx.Players.Find(dbGame.PlayerAId);
            dbCtx.Players.Find(dbGame.PlayerBId);
            var b1= dbCtx.Boards.Find(dbGame.PlayerA.BoardId);
            var b2 = dbCtx.Boards.Find(dbGame.PlayerB.BoardId);
                
            dbCtx.Bombs.Where(x => x.BoardId == b1.BoardId).Load();
            dbCtx.Bombs.Where(x => x.BoardId == b2.BoardId).Load();
                
            var ships = dbCtx.ShipsOnBoard.Where(x => x.BoardId == b1.BoardId).ToList();
            dbCtx.ShipsOnBoard.Where(x => x.BoardId == b2.BoardId).Load();
            
            foreach (var s in ships)
            {
                dbCtx.Ships.Find(s.ShipId);
            }
                
            // In existing game only boards (and board related things) and NextMoveByA could be updated
            dbGame.NextMoveByA = battleships.NextMoveByA;
            foreach ( var (boardInGame, boardInDb) in new List<(GameBrain.Board, Domain.Board)> 
                {(battleships.PlayerA.Board, dbGame.PlayerA.Board), (battleships.PlayerB.Board, dbGame.PlayerB.Board)})
            {
                foreach (var shipOnBoard in boardInDb.ShipsOnBoard)
                {
                    shipOnBoard.IsSunken = boardInGame.Ships
                        .Find(ship => shipOnBoard.StartRow == ship.AnchorPosition!.Row &&
                                      shipOnBoard.StartColumn == ship.AnchorPosition!.Column)!.IsSunken;
                }
            
                for (var i = boardInDb.Bombs.Count; i < boardInGame.BombCells.Count; i++)
                {
                    var bombCell = boardInGame.BombCells[i];
                    boardInDb.Bombs.Add(new Bomb
                    {
                        BoardId = boardInDb.BoardId,
                        Row = bombCell.Row,
                        Column = bombCell.Column,
                        Order = i
                    });
                }

                dbCtx.SaveChanges();
            }
        }
        
        private static void SaveNewGameIntoDatabase(Battleships battleships, BattleshipsDbContext dbCtx)
        {
            var uniqueShips = battleships.GameSessionShips.Select(pair => new Domain.Ship
            {
                Name = pair.Key.Name,
                Size = pair.Key.Size
            }).ToArray();

            for (var i = 0; i < uniqueShips.Length; i++)
            {
                var ship = uniqueShips[i];
                var dbShip = dbCtx.Ships.FirstOrDefault(s => s.Name == ship.Name && s.Size == ship.Size);
                if (dbShip == null) dbCtx.Ships.Add(ship);
                else uniqueShips[i] = dbShip;
            }

            dbCtx.SaveChanges();

            GameOption gameOptions;
            if (battleships.GameOptionId > 0)
            {
                gameOptions = dbCtx.GameOptions.Find(battleships.GameOptionId);
            }
            else
            {
                gameOptions = battleships.GetSessionOptions();

                var dbGameOption = dbCtx.GameOptions.FirstOrDefault(opt => opt.Name == gameOptions.Name
                                                                           && opt.BoardHeight == gameOptions.BoardHeight
                                                                           && opt.BoardWidth == gameOptions.BoardWidth
                                                                           && opt.ShipsCanTouch ==
                                                                           gameOptions.ShipsCanTouch
                                                                           && opt.NextMoveAfterHit ==
                                                                           gameOptions.NextMoveAfterHit);
                if (dbGameOption == null) dbCtx.GameOptions.Add(gameOptions);
                else gameOptions = dbGameOption;

                dbCtx.SaveChanges();
            }

            var gameOptionShips = battleships.GameSessionShips.Select((shipAmountPair, i) => 
                new GameOptionShip {
                    GameOptionId = gameOptions.GameOptionId,
                    Ship = uniqueShips[i],
                    Amount = shipAmountPair.Value,
                }).ToArray();

            for (var i = 0; i < gameOptionShips.Length; i++)
            {
                var optShip = gameOptionShips[i];
                var dbShip = dbCtx.GameOptionShips.FirstOrDefault(s => s.Ship == optShip.Ship 
                                                                       && s.Amount == optShip.Amount
                                                                       && s.GameOptionId == optShip.GameOptionId);
                if (dbShip == null) dbCtx.GameOptionShips.Attach(optShip);
                else gameOptionShips[i] = dbShip;
            }
            dbCtx.SaveChanges();
            

            var boardA = battleships.PlayerA.Board.SerializeBoard(uniqueShips);
            var boardB = battleships.PlayerB.Board.SerializeBoard(uniqueShips);

            dbCtx.Boards.Add(boardA);
            dbCtx.Boards.Add(boardB);
            dbCtx.SaveChanges();
            
            var playerA = battleships.PlayerA.SerializePlayer();
            playerA.Board = boardA;
            var playerB = battleships.PlayerB.SerializePlayer();
            playerB.Board = boardB;

            dbCtx.Players.Add(playerA);
            dbCtx.Players.Add(playerB);
            dbCtx.SaveChanges();

            var game = new Game
            {
                Name = battleships.GameName,
                GameOption = gameOptions,
                PlayerA = playerA,
                PlayerB = playerB,
                NextMoveByA = battleships.NextMoveByA
            };
            
            dbCtx.Games.Add(game);
            dbCtx.SaveChanges();

            battleships.GameId = game.GameId;
        }
        
        public static int SaveGameOptionIntoDatabase(GameSessionParams sessionParams)
        {
            using var dbCtx = new BattleshipsDbContext(DbOptions);
            var gameOption = new GameOption
            {
                BoardHeight = sessionParams.BoardHeight,
                BoardWidth = sessionParams.BoardWidth,
                Name = sessionParams.GameOptionName,
                ShipsCanTouch = sessionParams.ShipsCanTouch,
                NextMoveAfterHit = sessionParams.NextMoveAfterHit,
            };
                
            dbCtx.GameOptions.Add(gameOption);
            dbCtx.SaveChanges();
                
            var dbGameOptionShips = sessionParams.GameSessionShips.Select((shipAmountPair, i) => new GameOptionShip
            {
                GameOptionId = gameOption.GameOptionId,
                ShipId = dbCtx.Ships.FirstOrDefault(ship => 
                        ship.Name == shipAmountPair.Key.Name
                        && ship.Size == shipAmountPair.Key.Size)
                    ?.ShipId,
                Amount = shipAmountPair.Value,
            }).ToArray();

            foreach (var optShip in dbGameOptionShips)
            {
                dbCtx.GameOptionShips.Attach(optShip);
            }
            dbCtx.SaveChanges();

            return gameOption.GameOptionId;
        }
        
    }
}