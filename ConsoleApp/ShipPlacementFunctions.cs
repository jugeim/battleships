﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Domain.Enums;
using GameBrain;
using GameConsoleUI;
using MenuSystem;
using MenuSystem.MenuItems;
using MenuSystem.View;

namespace ConsoleApp
{
    public static class ShipPlacementFunctions
    {
        public static ExecutionResults ChooseShipAndPlace(Battleships game, 
            Dictionary<bool, Dictionary<Ship, int>> availableShips, bool shouldUseRandom, 
            MenuView menuView, BattleshipBoardDrawer gameView)
        {
            var shipsToChoose = availableShips[game.NextMoveByA];
            
            var shipsList = new Menu(MenuLevel.Level1, "Ships", menuView);
            shipsList.RemoveDefaultMenuItems();
            var nr = 1;
            foreach (var (ship, count) in shipsToChoose)
            {
                shipsList.AddMenuItem(new MenuItem(nr.ToString(), $"{ship.Name} ({count})", () =>
                {
                    if (shipsToChoose[ship] > 0)
                    {
                        if (shouldUseRandom)
                        {
                            PlaceShipRandomly(game, ship);
                        }
                        else
                        {
                            var shouldExit = PlaceShip(game, ship, gameView);
                            if (shouldExit) return new ExecutionResults();
                        }
                        shipsToChoose[ship]--;
                        return new ExecutionResults().WithCommand(NavigationCommand.ToMain);
                    }
                    return new ExecutionResults();
                }));
                nr++;
            }
            shipsList.AddMenuItem(new MenuItemEscape("R", "Return", NavigationCommand.ToMain));


            return shipsList.RunMenu();
        }

        public static ExecutionResults PlaceShips(Battleships game, Dictionary<bool, Dictionary<Ship, int>> availableShips,
            BattleshipBoardDrawer gameView)
        {
            var shipsToPlace = new Dictionary<Ship, int>(availableShips[game.NextMoveByA]);
            
            foreach (var (ship, count) in shipsToPlace)
            {
                for (int j = 0; j < count; j++)
                {
                    var shouldExit = PlaceShip(game, ship, gameView);
                    if (shouldExit) return new ExecutionResults();
                    availableShips[game.NextMoveByA][ship]--;
                }
            }
                
            return new ExecutionResults();
        }
        
        private static bool PlaceShip(Battleships game, Ship ship, BattleshipBoardDrawer gameView)
        {
            Console.Clear();
            gameView.Reset();

            var shipOnBoard = game.PlaceShip(ship, 0, 0, ELayout.Horizontal)!;
            var shouldExit = PositionShipOnBoard(game, shipOnBoard, gameView);
            Console.Clear();
            
            return shouldExit;
        }

        public static void PlaceShipRandomly(Battleships game, Ship ship)
        {
            var board = game.NextMoveByA ? game.PlayerA.Board : game.PlayerB.Board;

            var rand = new Random();
            var rowPool = Enumerable.Range(0, board.Rows).OrderBy(x => rand.Next()).ToList();
            var colPool = Enumerable.Range(0, board.Columns).ToList();

            foreach (var randRow in rowPool)
            {
                foreach (var randCol in colPool.OrderBy(x => rand.Next()).ToList())
                {
                    var randLayout = (ELayout) rand.Next(2);
                    var randShip = new Ship(ship, randRow, randCol, randLayout);
                    var canShipBePlaced = board.CanShipBePlaced(randShip, false, game.ShipsCanTouch);
                    if (!canShipBePlaced)
                    {
                        randShip.SwitchLayout();
                        canShipBePlaced = board.CanShipBePlaced(randShip, false, game.ShipsCanTouch);
                    }
                    if (canShipBePlaced)
                    {
                        board.PlaceShip(randShip);
                        return;
                    }
                }
            }
            throw new Exception("Can not place ship");
        }

        public static ExecutionResults PlaceShipsRandomly(Battleships game, Dictionary<bool, Dictionary<Ship, int>> availableShips)
        {
            int iterationsCount = 0;
            while (iterationsCount < 100)
            {
                var shipsToPlace = new Dictionary<Ship, int>(availableShips[game.NextMoveByA]);

                var i = 0;
                foreach (var (ship, count) in shipsToPlace)
                {
                    for (int j = 0; j < count; j++)
                    {
                        try
                        {
                            PlaceShipRandomly(game, ship);
                            availableShips[game.NextMoveByA][ship]--;
                            i++;
                        }
                        catch (Exception)
                        {
                            for (int k = 0; k < i; k++)
                            {
                                UndoLastShipPlacement(game, availableShips);
                            }
                        }
                    }
                }
                iterationsCount++;
                if (!shipsToPlace.Values.ToList().TrueForAll(x => x == 0)) continue;
                return new ExecutionResults();
            }
            throw new Exception("Ships can not be placed on board (or random algorithm could not perform that)");
        }

        public static ExecutionResults UndoLastShipPlacement(Battleships game,
            Dictionary<bool, Dictionary<Ship, int>> availableShips)
        {
            var ship = game.NextMoveByA ? game.PlayerA.Board.RemoveLastShip() : game.PlayerB.Board.RemoveLastShip();
            if (ship == null) return new ExecutionResults();
            availableShips[game.NextMoveByA][ship]++;
            return new ExecutionResults();
        }
        
        private static bool PositionShipOnBoard(Battleships game, Ship ship, BattleshipBoardDrawer gameView)
        {
            var title = "Board navigation: AWSD \n Ship placement: arrows to move, space to rotate, enter to place";
            var board = game.NextMoveByA ? game.PlayerA.Board : game.PlayerB.Board;
            var listener = new ArrowListener(true);
            ConsoleKey commandKey;
            Console.WriteLine(ship);
            var isPlacedCorrectly = board.CanShipBePlaced(ship, false, game.ShipsCanTouch);
            var footer = $"Current ship is {ship.Name} (1x{ship.Size})";
            
            do
            {
                gameView.DrawBoard(board, title, footer);
                var newCommand = listener.Listen();
                int shipRow, shipCol;
                
                if (newCommand == ConsoleKey.A)
                    gameView.MoveLeft();
                else if (newCommand == ConsoleKey.D)
                    gameView.MoveRight(board.Columns);
                else if (newCommand == ConsoleKey.W)
                    gameView.MoveUp();
                else if (newCommand == ConsoleKey.S) 
                    gameView.MoveDown(board.Rows);
                else if (newCommand == ConsoleKey.LeftArrow)
                {
                    isPlacedCorrectly = board.MoveLastShip("l", out shipRow, out shipCol, game.ShipsCanTouch);
                    if (shipCol - ship.Size < gameView.CurrentColumn) gameView.MoveLeft();
                }
                else if (newCommand == ConsoleKey.RightArrow)
                {
                    isPlacedCorrectly = board.MoveLastShip("r", out shipRow, out shipCol, game.ShipsCanTouch);
                    if (shipCol + ship.Size >= gameView.CurrentColumn + gameView.BoardColumnsLimit) gameView.MoveRight(board.Rows);
                }
                else if (newCommand == ConsoleKey.UpArrow)
                {
                    isPlacedCorrectly = board.MoveLastShip("u", out shipRow, out shipCol, game.ShipsCanTouch);
                    if (shipRow - ship.Size < gameView.CurrentRow) gameView.MoveUp();
                }
                else if (newCommand == ConsoleKey.DownArrow)
                {
                    isPlacedCorrectly = board.MoveLastShip("d", out shipRow, out shipCol, game.ShipsCanTouch);
                    if (shipRow + ship.Size >= gameView.CurrentRow + gameView.BoardRowsLimit) gameView.MoveDown(board.Columns);
                }
                else if (newCommand == ConsoleKey.Spacebar)
                    isPlacedCorrectly = board.MoveLastShip("s", out shipRow, out shipCol, game.ShipsCanTouch);
                else if (newCommand == ConsoleKey.Backspace)
                {
                    board.RemoveLastShip();
                    isPlacedCorrectly = true;
                }
                else if (newCommand == ConsoleKey.Tab)
                    isPlacedCorrectly = PlaceShipByCoordinates(game, ship, title, isPlacedCorrectly, gameView);

                commandKey = newCommand;
                Console.Clear();

            } while (!(commandKey == ConsoleKey.Enter && isPlacedCorrectly 
                       || commandKey == ConsoleKey.Backspace));
            gameView.DrawBoard(board, title, false);
            return commandKey == ConsoleKey.Backspace;
        }
        
        private static bool PlaceShipByCoordinates(Battleships game, Ship ship, string title, bool isPlacedCorrectly, 
            BattleshipBoardDrawer gameView)
        {
            var board = game.NextMoveByA ? game.PlayerA.Board : game.PlayerB.Board;
            var footer = $"Current ship is {ship.Name} (1x{ship.Size})\n" +
                         $"Insert anchor position coordinates and layout (row, col, H(orizontal)/V(ertical)): ";
            bool isReady;
            do
            {
                Console.Clear();
                gameView.DrawBoard(board, title, footer);
                var firstKey = Console.ReadKey(); // if try to switch back with Tab
                if (firstKey.Key == ConsoleKey.Tab) return isPlacedCorrectly;
                var userInput = Console.ReadLine() ?? "";
                userInput = Regex.IsMatch(firstKey.KeyChar.ToString(), "[\\w-[_]]")
                    ? firstKey.KeyChar + userInput
                    : userInput;
                Ship? shipOnBoard = null;
                var prevShip = board.RemoveLastShip();
                if (userInput.Trim() == "")
                {
                    board.PlaceShip(prevShip!);
                }
                else
                {
                    var values = Regex.Split(userInput, @"[\W_]+");

                    var layout = values.Length < 3 
                                 || Regex.IsMatch(values[2].ToLower().Trim(), @"^[\s]*$|h(or(iz(on(tal)?)?)?)?") 
                        ? ELayout.Horizontal : ELayout.Vertical;
                    
                    if (values.Length > 1 && int.TryParse(values[0].Trim(), out var row) &&
                        int.TryParse(values[1].Trim(), out var col))
                    {
                        shipOnBoard = board.PlaceShip(ship, row, col, layout);
                        gameView.SetCurrentPosition(row, col, board.Rows, board.Columns);
                    }
                    else
                    {
                        board.PlaceShip(prevShip!);
                    }
                }

                Console.Clear();
                gameView.DrawBoard(board, title, footer);
                
                isPlacedCorrectly = board.CanShipBePlaced(shipOnBoard, true, game.ShipsCanTouch);
                var shouldSave = false;
                if (!isPlacedCorrectly)
                {
                    Console.WriteLine("Ship can not be placed on this position");
                    board.RemoveLastShip();
                    board.PlaceShip(prevShip!);
                    Console.ReadKey();
                }
                else
                {
                    Console.WriteLine("Save position? (y/N)");
                    userInput = Console.ReadLine() ?? "";
                    shouldSave = Regex.IsMatch(userInput.ToLower(), @"^(y(es)?)?$");
                }

                isReady = shipOnBoard != null && shouldSave && isPlacedCorrectly;

            } while (!isReady);
            
            return true;
        }
    }
    
}