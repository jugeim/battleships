﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp
{
    public class ArrowListener
    {
        private readonly List<ConsoleKey> _enabledKeys = new List<ConsoleKey>
        {
            ConsoleKey.Enter,
            ConsoleKey.Backspace,
            ConsoleKey.LeftArrow,
            ConsoleKey.RightArrow,
            ConsoleKey.UpArrow,
            ConsoleKey.DownArrow,
        };

        public ArrowListener(bool shouldEnableAdditionalKeys)
        {
            if (shouldEnableAdditionalKeys)
            {
                _enabledKeys.AddRange(new[]
                {
                    ConsoleKey.Tab,
                    ConsoleKey.Spacebar,
                    ConsoleKey.A,
                    ConsoleKey.W,
                    ConsoleKey.S,
                    ConsoleKey.D
                });
            }
        }

        public ConsoleKey Listen()
        {
            while (true)
            {
                var keyInfo = Console.ReadKey();

                if (!_enabledKeys.Contains(keyInfo.Key)) continue;

                switch (keyInfo.Key)
                {
                    case ConsoleKey.Enter:
                    case ConsoleKey.Backspace:
                    case ConsoleKey.UpArrow:
                    case ConsoleKey.DownArrow:
                    case ConsoleKey.LeftArrow:
                    case ConsoleKey.RightArrow:
                    case ConsoleKey.W:
                    case ConsoleKey.S:
                    case ConsoleKey.A:
                    case ConsoleKey.D:
                    case ConsoleKey.Spacebar:
                    case ConsoleKey.Tab:
                        return keyInfo.Key;
                    default:
                        throw new Exception("should not be here");
                }
            }
        }
    }
}