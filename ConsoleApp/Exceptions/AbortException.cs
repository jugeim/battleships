﻿using System;

namespace ConsoleApp.Exceptions
{
    public class AbortException : Exception
    {
        public AbortException(string message) : base(message)
        {
            
        }
    }
}