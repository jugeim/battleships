﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using MenuSystem;

namespace ConsoleApp
{
    public static class CommonFunctions
    {
        public static ExecutionResults UpdateStringValue(out string value, string oldValue, string valueName, int minLength, int maxLength)
        {
            var result = new ExecutionResults {StrValue = oldValue};
            value = oldValue;
            
            var (newValue, shouldUpdate) = GetNewStringValue(valueName, minLength, maxLength);
            if (shouldUpdate)
            {
                result.StrValue = newValue;
                value = newValue;
            }
            return result;
        }
        
        public static ExecutionResults GenerateRandomStringValue(out string value, int minLength, int maxLength)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            Random rnd = new Random();
            
            var length = rnd.Next(minLength, maxLength);
            var newValue = new string(Enumerable.Repeat(chars, length)
                .Select(s => s[rnd.Next(s.Length)]).ToArray());

            value = newValue;
            return new ExecutionResults {StrValue = newValue};
        }
        
        public static ExecutionResults UpdateUniqueStringValue(out string value, string oldValue, string valueName, 
            int minLength, int maxLength, ICollection<string> occupiedValues)
        {
            var result = new ExecutionResults {StrValue = oldValue};
            value = oldValue;
            bool canUpdate = false;
            bool shouldUpdate = false;
            string newValue = "";
            while (!canUpdate)
            {
                (newValue, shouldUpdate) = GetNewStringValue(valueName, minLength, maxLength);
                canUpdate = !occupiedValues.Contains(newValue.Trim());
                
                if (!canUpdate)
                {
                    ConsoleClearLine();
                    Console.Write("name is already occupied, try another");
                    Thread.Sleep(1000);
                }
            }
            if (shouldUpdate)
            {
                result.StrValue = newValue;
                value = newValue;
            }
            return result;
        }
        
        private static (string value, bool shouldUpdate) GetNewStringValue(string valueName, int min, int max)
        {
            var firstAttempt = true;
            while (true)
            {
                Console.Write($"Insert new {valueName} with length in range {min}-{max - 1}{(firstAttempt ? "" : " (or inset e to exit)")}:>");
                var userInput = Console.ReadLine() ?? "";
                if (userInput.Trim().ToLower() =="e" || string.IsNullOrWhiteSpace(userInput))
                {
                    return ("", false);
                }

                if (userInput.Length >= min && userInput.Length < max)
                {
                    return (userInput, true);
                }
                ConsoleClearLine();
                Console.Write(
                    $"{new string(' ', firstAttempt ? 24 : 45)} => \"{userInput}\" is too {(userInput.Length >= min ? "long" : "short")}");
                Thread.Sleep(1000);
                Console.WriteLine();
                ConsoleClearLine();
                firstAttempt = false;
            }
        }


        public static ExecutionResults UpdateIntValue(out int value, int oldValue, string valueName, int minValue, int maxValue)
        {
            var result = new ExecutionResults {StrValue = oldValue.ToString()};
            value = oldValue;
            
            var (newValue, shouldUpdate) = GetNewIntValue(valueName, minValue, maxValue);
            if (shouldUpdate)
            {
                result.StrValue = newValue.ToString();
                value = newValue;
            }
            return result;
        }

        private static (int value, bool shouldUpdate) GetNewIntValue(string valueName, int min, int max)
        {
            var firstAttempt = true;
            while (true)
            {
                Console.Write($"Insert new {valueName} in range {min}-{max-1}{(firstAttempt ? "" : " (or inset e to exit)")}:>");
                var userInput = Console.ReadLine() ?? "";
                if (userInput.Trim().ToLower().Equals("e"))
                {
                    return (0, false);
                }
                if (int.TryParse(userInput.Trim(), out var newValue))
                {
                    if (newValue >= min && newValue < max)
                    {
                        return (newValue, true);
                    }
                    ConsoleClearLine();
                    Console.Write(
                        $"{new string(' ', firstAttempt ? 24 : 45)} => Value {newValue} is too {(newValue >= min ? "big" : "small")}");
                    Thread.Sleep(1000);
                    Console.WriteLine();
                }
                ConsoleClearLine();
                firstAttempt = false;
            }
        }
        
        public static ExecutionResults UpdateEnumValue<T>(out T value, T oldValue, string valueName) where T : Enum
        {
            var result = new ExecutionResults {StrValue = oldValue.ToString()};
            value = oldValue;
            
            var (newValue, shouldUpdate) = GetNewEnumValue<T>(valueName);
            if (shouldUpdate)
            {
                result.StrValue = newValue.ToString();
                value = newValue;
            }
            return result;
        }

        private static (T value, bool shouldUpdate) GetNewEnumValue<T>(string valueName) where T : Enum
        {
            var firstAttempt = true;
            while (true)
            {
                Console.Clear();
                var count = 0;
                foreach (var opt in Enum.GetValues(typeof(T)))
                {
                    Console.WriteLine($"{count}) {opt}");
                    count++;
                }
                Console.Write($"Insert new {valueName} ordinal number {(firstAttempt ? "" : " (or inset e to exit)")}:>");
                var userInput = Console.ReadLine() ?? "";
                if (userInput.Trim().ToLower().Equals("e"))
                {
                    return (default, false)!;
                }
                if (int.TryParse(userInput.Trim(), out var newValue))
                {
                    if (newValue >= 0 && newValue < count)
                    {
                        return ((T)Enum.Parse(typeof(T), newValue.ToString(), true), true);
                    }
                    ConsoleClearLine();
                    Console.Write(
                        $"{new string(' ', firstAttempt ? 24 : 45)} => Value {newValue} is too not in range");
                    Thread.Sleep(1000);
                    Console.WriteLine();
                }
                ConsoleClearLine();
                firstAttempt = false;
            }
        }

        public static void ClearAndWaitForKey()
        {
            Console.Clear();
            Console.WriteLine("Press any key to continue.");
            Console.ReadKey();
        }

        public static void ConsoleClearLine()
        {
            Console.SetCursorPosition(0, Console.CursorTop > 0 ? Console.CursorTop - 1 : 0);
            Console.Write(new string(' ', Console.WindowWidth)); 
            Console.SetCursorPosition(0, Console.CursorTop - 1);
        }

        public static void ConsoleErrorMessage(string msg)
        {
            var colour = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(msg);
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
            Console.ForegroundColor = colour;
        }
    }
}