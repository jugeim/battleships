﻿namespace AI
{
    public enum EDirection
    {
        Undefined,
        Up, 
        Down, 
        Left, 
        Right
    }
}