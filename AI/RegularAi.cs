﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Enums;


namespace AI
{
    public class RegularAi
    {

        public bool IsGameKnowledgeSet { get; set; } = false;
        private int _boardHeight;
        private int _boardWidth;
        private Dictionary<int, int> _gameShips = new Dictionary<int, int>();
        private EShipsCanTouch _shipsCanTouch;
        private Random _rnd = new Random();

        private List<int> _fullRows = new List<int>();
        private HashSet<(int Row, int Col)> _checkedCells = new HashSet<(int, int)>();

        private (int Row, int Col) _lastHit;
        private List<(int Row, int Col)> HitQueue = new List<(int, int)>();
        private EDirection Direction { get; set; }
        private bool IsDirectionKnown { get; set; }

        public RegularAi()
        {
            _boardHeight = 0;
            _boardWidth = 0;
            _shipsCanTouch = EShipsCanTouch.No;
        }

        public RegularAi(int rows, int cols, EShipsCanTouch shipsCanTouch, Dictionary<int, int> gameShips)
        {
            _boardHeight = rows;
            _boardWidth = cols;
            _shipsCanTouch = shipsCanTouch;
            _gameShips = gameShips;
        }

        public void UpdateAiKnowledge(GameSessionPublicParameters parameters)
        {
            IsGameKnowledgeSet = true;
            _boardHeight = parameters.BoardHeight;
            _boardWidth = parameters.BoardWidth;
            _shipsCanTouch = parameters.ShipsCanTouch;
            _gameShips = parameters.GameShips;
            _checkedCells = parameters.PlacedBombs;
        }

        public void SetAiKnowledgeByPlacedBombs(Dictionary<(int, int), bool> movesImpact)
        {
            foreach (var ((row, col), hasHit) in movesImpact)
            {
                HitCoordinates(row, col);
                ProcessMoveResults(hasHit);
            }
        }

        public void ProcessMoveResults(bool wasHit)
        {
            if (wasHit) HitQueue.Add(_lastHit);
            if (HitQueue.Count > 1) IsDirectionKnown = true;
            if (IsDirectionKnown && (_shipsCanTouch == EShipsCanTouch.No || _shipsCanTouch == EShipsCanTouch.Corner))
            {
                var isHorizontalDirection = Direction == EDirection.Right || Direction == EDirection.Left;
                if (HitQueue.Count == 2)
                {
                    MarkParallelAsChecked(HitQueue[0], isHorizontalDirection);
                }
                MarkParallelAsChecked(HitQueue.Last(), isHorizontalDirection);
                
            }
            var knownLength = HitQueue.Count;
            if (knownLength == _gameShips.Keys.Max())
            {
                _gameShips[knownLength]--;
                if (_gameShips[knownLength] == 0) _gameShips.Remove(knownLength);

                HitQueue.Clear();
                Direction = EDirection.Undefined;
                IsDirectionKnown = false;
            }
        }

        private void MarkParallelAsChecked((int row, int col) coord, bool isHorizontalPlacement)
        {
            var (row, col) = coord;
            if (isHorizontalPlacement)
            {
                _checkedCells.Add((row - 1, col));
                _checkedCells.Add((row + 1, col));
            }
            else
            {
                _checkedCells.Add((row, col - 1));
                _checkedCells.Add((row, col + 1));
            }
        }

        public (int row, int col) GiveMoveCoordinates()
        {
            if (HitQueue.Count == 0)
            {
                return GetAvailableRandom();
            } 
            if (Direction == EDirection.Undefined)
            {
                Direction++;
                return GetAvailableCoordinatesAround();
            }
            return IsDirectionKnown ? GetAvailableInDirection(false) : GetAvailableCoordinatesAround();
        }

        private (int, int) HitCoordinates(int row, int col)
        {
            _lastHit = (row, col);
            _checkedCells.Add((row, col));
            return (row, col);
        }

        private void CheckForFullRows()
        {
            for (int row = 0; row < _boardHeight; row++)
            {
                if (_checkedCells.Count(coord => coord.Row == row) >= _boardWidth) _fullRows.Add(row);
            }
        }

        private (int, int) GetAvailableRandom()
        {
            CheckForFullRows();
            int row, col;
            do
            {
                row = _rnd.Next(_boardHeight);
            } while (_fullRows.Contains(row));

            do
            {
                col = _rnd.Next(_boardWidth);
            } while (_checkedCells.Contains((row, col)));
            
            return HitCoordinates(row, col);
        }

        private void SetOppositeDirection()
        {
            Direction = Direction switch
            {
                EDirection.Up => EDirection.Down,
                EDirection.Down => EDirection.Up,
                EDirection.Left => EDirection.Right,
                EDirection.Right => EDirection.Left,
                _ => EDirection.Undefined
            };
        }

        private (int, int) GetOnOppositeSideOrRandom(bool isOnOppositeDirection)
        {
            if (isOnOppositeDirection)
            {
                var knownLength = HitQueue.Count;
                _gameShips[knownLength]--;
                if (_gameShips[knownLength] == 0) _gameShips.Remove(knownLength);
                
                HitQueue.Clear();
                Direction = EDirection.Undefined;
                IsDirectionKnown = false;
                return GetAvailableRandom();
            }
            SetOppositeDirection();
            return GetAvailableInDirection(true);
        }

        private (int, int) GetHitQueueOppositeSide()
        {
            var row = HitQueue.Min(x => x.Row);
            var col = HitQueue.Min(x => x.Col);
            if ((row, col) == HitQueue.Last())
            {
                row = HitQueue.Max(x => x.Row);
                col = HitQueue.Max(x => x.Col);
            }
            return (row, col);
        }

        private (int, int) GetAvailableInDirection(bool isOnOppositeDirection)
        {
            var (row, col) = HitQueue.Last();
            
            if (_lastHit != (row, col) || isOnOppositeDirection)
            {
                if (!isOnOppositeDirection) SetOppositeDirection();
                (row, col) = GetHitQueueOppositeSide();
            }
            
            switch (Direction)
            {
                case EDirection.Up:
                {
                    var newRow = row - 1;
                    return newRow < 0 || _checkedCells.Contains((newRow, col))
                        ? GetOnOppositeSideOrRandom(isOnOppositeDirection) 
                        : HitCoordinates(newRow, col);
                }
                case EDirection.Down:
                {
                    var newRow = row + 1;
                    return newRow >= _boardHeight || _checkedCells.Contains((newRow, col))
                        ? GetOnOppositeSideOrRandom(isOnOppositeDirection) 
                        : HitCoordinates(newRow, col);
                }
                case EDirection.Left:
                {
                    var newCol = col - 1;
                    return newCol < 0 || _checkedCells.Contains((row, newCol))
                        ? GetOnOppositeSideOrRandom(isOnOppositeDirection) 
                        : HitCoordinates(row, newCol);
                }
                case EDirection.Right:
                {
                    var newCol = col + 1;
                    return newCol >= _boardWidth || _checkedCells.Contains((row, newCol))
                        ? GetOnOppositeSideOrRandom(isOnOppositeDirection) 
                        : HitCoordinates(row, newCol);
                }
                default:
                    HitQueue.Clear();
                    Direction = EDirection.Undefined;
                    IsDirectionKnown = false;
                    return GetAvailableRandom();
            }
        }

        private (int, int) GetAvailableCoordinatesAround()
        {
            var (row, col) = HitQueue.Last();

            if (Direction == EDirection.Up)
            {
                var newRow = row - 1;
                if (newRow < 0 || _checkedCells.Contains((newRow, col))) Direction++;
                else return HitCoordinates(newRow, col);
            }
            if (Direction == EDirection.Down)
            {
                var newRow = row + 1;
                if (newRow >= _boardHeight || _checkedCells.Contains((newRow, col))) Direction++;
                else return HitCoordinates(newRow, col);
            }
            if (Direction == EDirection.Left)
            {
                var newCol = col - 1;
                if (newCol < 0 || _checkedCells.Contains((row, newCol))) Direction++;
                else return HitCoordinates(row, newCol);
            }
            if (Direction == EDirection.Right)
            {
                var newCol = col + 1;
                if (newCol >= _boardWidth || _checkedCells.Contains((row, newCol))) Direction = 0;
                else return HitCoordinates(row, newCol);
            }
            return GetAvailableRandom();
        }
    }
}