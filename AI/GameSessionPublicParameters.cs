﻿using System.Collections.Generic;
using Domain.Enums;

namespace AI
{
    public class GameSessionPublicParameters
    {
        public int BoardHeight { get; set; }
        public int BoardWidth { get; set; }
        public Dictionary<int, int> GameShips { get; set; } = new Dictionary<int, int>();
        public EShipsCanTouch ShipsCanTouch { get; set; }
        
        public HashSet<(int, int)> PlacedBombs { get; set; } = new HashSet<(int, int)>();
    }
}