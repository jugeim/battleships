﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace GameConsoleUI
{
    public class BoardDrawingParameters
    {
        public int Rows { get; set; }
        public int Columns { get; set; }
        public int FirstRow { get; set; }
        public int FirstColumn { get; set; }
        public int BoardWidth { get; set; }
        public int VerticalEnumeratorWidth { get; set; }
        
        public IEnumerable<int> RowRange => Enumerable.Range(FirstRow, Rows);
        public IEnumerable<int> ColumnRange => Enumerable.Range(FirstColumn, Columns);
        
    }
}