﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameBrain;

namespace GameConsoleUI
{
    public class ColoredBoardDrawer : BattleshipBoardDrawer
    {
        protected override int CellWidth { get; } = 2;
        protected override int SeparationWidth { get; } = 9;
        protected override double SeparationHeight { get; } = 1.6;

        public override void DrawBoard(Board board, string title, bool isOpponentBoard)
        {
            var parameters = CalculateBoardDrawingParameters(board);

            var lines = GetBoardLines(board, parameters, title, isOpponentBoard);

            foreach (var line in lines)
            {
                if (line[parameters.VerticalEnumeratorWidth] == '|')
                {
                    WriteColorizedLine(line);
                }
                else
                {
                    Console.WriteLine(line);
                }
            }
        }

        

        public override bool DrawTwoBoards(Battleships game)
        {
            var params1= CalculateGameDrawingParameters(game).Item1;
            var leftBorderPos = params1.VerticalEnumeratorWidth;
            var lines = ComposeGameLines(game);

            foreach (var line in lines)
            {
                if (line.Length > leftBorderPos && line[leftBorderPos] == '|')
                {
                    WriteColorizedLine(line);
                }
                else
                {
                    Console.WriteLine(line);
                }
            }
            return lines.Count > 1;
        }

        private void WriteColorizedLine(string line)
        {
            var mainColor = Console.BackgroundColor;
            var lastColor = mainColor;
            foreach (var cell in line)
            { 
                Console.BackgroundColor = cell switch
                {
                    '~' => ConsoleColor.Blue,
                    'H' => ConsoleColor.Red,
                    'M' => ConsoleColor.Yellow,
                    'S' => ConsoleColor.Black,
                    'E' => ConsoleColor.Red,
                    '|' => mainColor,
                    _ => lastColor
                };
                Console.Write(cell);
                lastColor = Console.BackgroundColor;
            }
            Console.WriteLine();
        }

        protected override string GetBoardRowLine(Board board, int row, BoardDrawingParameters parameters, bool isOpponentBoard)
        {
            var line = new StringBuilder();
            var first = parameters.FirstColumn;
            if (row < board.Rows)
            {
                var paddingLength = parameters.VerticalEnumeratorWidth - row.ToString().Length > 0
                    ? parameters.VerticalEnumeratorWidth - row.ToString().Length
                    : 0;
                    line.Append($"{GetPadding(paddingLength)}{row}");
                foreach (int col in parameters.ColumnRange)
                {
                    line.Append($"{(col % 10 == 0 || col == first ? "|" : " ")}{CellString(board.GetCellState(row, col), isOpponentBoard)}");
                }

                line.Append('|');
            }

            return line.ToString();
        }

        protected override List<string> GetBoardHorizontalEnumeration(BoardDrawingParameters parameters)
        {
            var result = new List<string>();
            var ordinalCounter = new StringBuilder(GetPadding(parameters.VerticalEnumeratorWidth));
            var mod10Counter = new StringBuilder(GetPadding(parameters.VerticalEnumeratorWidth));
            foreach (int col in parameters.ColumnRange)
            {
                ordinalCounter.Append($"{(col % 10 == 0 ? $"{((col - col % 10) % 1000) / 10, 2}" : "  ")}");
                mod10Counter.Append($"{col % 10, 2}");
            }

            ordinalCounter.Append(' ');
            mod10Counter.Append(' ');
            
            result.Add(ordinalCounter.ToString());
            result.Add(mod10Counter.ToString());
            return result;
        }

        protected override string GetBoardHorizontalSeparator(BoardDrawingParameters parameters)
        {
            var result = new StringBuilder(GetPadding(parameters.VerticalEnumeratorWidth));
            result.Append("+");
            foreach (var col in parameters.ColumnRange)
            {
                result.Append("--");
            }
            result.Remove(result.Length - 1, 1);
            result.Append("+");
            return result.ToString();
        }
    }
}