﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameBrain;

namespace GameConsoleUI
{
    public class MonochromeBoardDrawer : BattleshipBoardDrawer
    {
        protected override int CellWidth { get; } = 4;
        protected override int SeparationWidth { get; } = 6;
        
        protected override double SeparationHeight { get; } = 3;

        public override void DrawBoard(Board board, string title, bool isOpponentBoard)
        {
            var parameters = CalculateBoardDrawingParameters(board);

            var lines = GetBoardLines(board, parameters, title, isOpponentBoard);

            foreach (var line in lines)
            {
                Console.WriteLine(line);
            }
        }

        public override bool DrawTwoBoards(Battleships game)
        {
            var lines = ComposeGameLines(game);
            
            foreach (var line in lines)
            {
                Console.WriteLine(line);
            }

            return lines.Count > 1;
        }

        protected override string GetBoardRowLine(Board board, int row, BoardDrawingParameters parameters, bool isOpponentBoard)
        {
            var line = new StringBuilder();
            if (row < board.Rows)
            {
                var paddingLength = parameters.VerticalEnumeratorWidth - row.ToString().Length > 0
                    ? parameters.VerticalEnumeratorWidth - row.ToString().Length
                    : 0;
                line.Append($"{GetPadding(paddingLength)}{row}");
                foreach (var col in parameters.ColumnRange)
                {
                    line.Append($"|{CenteredString(CellString(board.GetCellState(row, col), isOpponentBoard), 3)}");
                }
                line.Append('|');
            }
            else
            {
                var boardWidth = parameters.ColumnRange.Count() * CellWidth;
                line.Append(GetPadding(parameters.VerticalEnumeratorWidth + boardWidth + 1));
            }

            return line.ToString();
        }
        
        protected override List<string> GetBoardHorizontalEnumeration(BoardDrawingParameters parameters)
        {
            var result = new StringBuilder(GetPadding(parameters.VerticalEnumeratorWidth));
            foreach (int col in parameters.ColumnRange)
            {
                result.Append($" {CenteredString(col.ToString(), CellWidth - 1)}");
            }
            result.Append(" ");
            return new List<string>{result.ToString()};
        }
        
        protected override string GetBoardHorizontalSeparator(BoardDrawingParameters parameters)
        {
            var result = new StringBuilder(GetPadding(parameters.VerticalEnumeratorWidth));
            foreach (var col in parameters.ColumnRange)
            {
                result.Append("+---");
            }
            result.Append("+");
            return result.ToString();
        }
    }
}