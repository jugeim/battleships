﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameBrain;

namespace GameConsoleUI
{
    public abstract class BattleshipBoardDrawer
    {
        public virtual int AvailableWidth => Console.WindowWidth;
        public virtual int AvailableHeight => Console.WindowHeight;
        public virtual int BoardColumnsLimit => CellWidth > 0 ?  (AvailableWidth - SeparationWidth) / CellWidth : 0;
        public virtual int BoardRowsLimit => (int) (CellWidth > 0 ? (AvailableHeight * SeparationHeight) / CellWidth : 0);

        protected abstract int CellWidth { get; }
        
        protected abstract int SeparationWidth { get; }
        protected abstract double SeparationHeight { get; }
        
        public int CurrentRow { get; set; }
        public int CurrentColumn { get; set; }
        public virtual int StepCols => BoardColumnsLimit / 2;
        public virtual int StepRows => BoardRowsLimit / 2;
        protected string BoardsHorizontalSeparator = "\t";

        public abstract void DrawBoard(Board board, string title, bool isOpponentBoard);
        public void DrawBoard(Board board, string title, string footer)
        {
            DrawBoard(board, title, false);
            Console.WriteLine(footer);
        }

        public abstract bool DrawTwoBoards(Battleships game);

        public void MoveLeft() => CurrentColumn = CurrentColumn >= StepCols ? CurrentColumn - StepCols : 0;
        public void MoveRight(int width) => CurrentColumn = CurrentColumn + StepCols < width ? CurrentColumn + StepCols : width - 1;
        public void MoveUp() => CurrentRow = CurrentRow >= StepCols ? CurrentRow - StepCols : 0;
        public void MoveDown(int height) => CurrentRow = CurrentRow + StepCols < height ? CurrentRow + StepRows : height - 1;
        
        public void SetCurrentPosition(int row, int col, int height, int width)
        {
            if (row > -1 && row < height)
            {
                CurrentRow = row < height ? row : height - 1;
            }
            if (col > -1 && col < width)
            {
                CurrentColumn = col < height ? col : width - 1;
            }  
        }

        public void Reset()
        {
            CurrentColumn = 0;
            CurrentRow = 0;
        }

        protected BoardDrawingParameters CalculateBoardDrawingParameters(Board board)
        {
            var parameters = new BoardDrawingParameters();
            
            parameters.Columns = board.Columns < BoardColumnsLimit ? board.Columns : BoardColumnsLimit - 1;
            parameters.FirstColumn = CurrentColumn + parameters.Columns > board.Columns && board.Columns - parameters.Columns >= 0
                ? board.Columns - parameters.Columns : CurrentColumn;
            parameters.Rows = board.Rows < BoardRowsLimit ? board.Rows : BoardRowsLimit - 1;
            parameters.FirstRow = CurrentRow + parameters.Rows > board.Rows && board.Rows - parameters.Rows >= 0 
                ? board.Rows - parameters.Rows : CurrentRow;
            parameters.VerticalEnumeratorWidth = $"{parameters.FirstRow + parameters.Rows - 1}".Length;
            parameters.BoardWidth = parameters.Columns * CellWidth + 1;

            return parameters;
        }

        protected (BoardDrawingParameters, BoardDrawingParameters) CalculateGameDrawingParameters(Battleships game)
        {
            var isATurn = game.NextMoveByA;

            var board1 = !isATurn ? game.PlayerA.Board : game.PlayerB.Board;
            var board2 = isATurn ? game.PlayerA.Board : game.PlayerB.Board;
            
            var parameters1 = new BoardDrawingParameters();
            var parameters2 = new BoardDrawingParameters();

            parameters1.Columns = board1.Columns;
            parameters2.Columns = board2.Columns;

            parameters1.FirstColumn = 0;
            parameters2.FirstColumn = 0;
            
            var targetRowsCount = Math.Max(board1.Rows, board2.Rows);

            parameters1.Rows = targetRowsCount;
            parameters2.Rows = targetRowsCount;
            
            parameters1.FirstRow = 0;
            parameters2.FirstRow = 0;
            
            parameters1.VerticalEnumeratorWidth = $"{parameters1.FirstRow + parameters1.Rows - 1}".Length;
            parameters2.VerticalEnumeratorWidth = $"{parameters2.FirstRow + parameters2.Rows - 1}".Length;
            
            parameters1.BoardWidth = parameters1.Columns * CellWidth + 1;
            parameters2.BoardWidth = parameters2.Columns * CellWidth + 1;

            return (parameters1, parameters2);
        }

        protected List<string> GetBoardLines(Board board, BoardDrawingParameters parameters, string title, bool isOpponentBoard)
        {
            var boardLines = new List<string>();
            boardLines.Add(GetBoardHeader(title, parameters));
            boardLines.AddRange(GetBoardHorizontalEnumeration(parameters));
            boardLines.Add(GetBoardHorizontalSeparator(parameters));
            boardLines.AddRange(parameters.RowRange.Select(
                row => GetBoardRowLine(board, row, parameters, isOpponentBoard)));
            boardLines.Add(GetBoardHorizontalSeparator(parameters));

            return boardLines;
        }

        protected List<string> ComposeGameLines(Battleships game)
        {
            var isATurn = game.NextMoveByA;

            var board1 = !isATurn ? game.PlayerA.Board : game.PlayerB.Board;
            var board2 = isATurn ? game.PlayerA.Board : game.PlayerB.Board;
            
            var (params1, params2) = CalculateGameDrawingParameters(game);
            var lines1 = GetBoardLines(board1, params1, "Opponent's board", true);
            var lines2 = GetBoardLines(board2, params2, "My board", false);
            var result = new List<string>();
            if (board1.Columns + board2.Columns <= BoardColumnsLimit)
            {
                result = GetBoardsInARow(lines1, lines2);
            } else if (board1.Columns <= BoardColumnsLimit && board2.Columns <= BoardColumnsLimit)
            {
                result = GetBoardsInAColumn(lines1, lines2);
            }
            else
            {
                result.Add("Boards are too big to show them at once. Please use separate views.");
            }

            return result;
        }

        private List<string> GetBoardsInARow(List<string> board1, List<string> board2)
        {
            var result = new List<string>();
            var line = new StringBuilder();
            for (int i = 0; i < board1.Count; i++)
            {
                line.Append(board1[i]).Append(BoardsHorizontalSeparator).Append(board2[i]);
                result.Add(line.ToString());
                line.Clear();
            }
            
            return result;
        }

        private List<string> GetBoardsInAColumn(IEnumerable<string> board1, IEnumerable<string> board2)
        {
            var result = new List<string>();

            result.AddRange(board1.Where(s => !string.IsNullOrEmpty(s)).ToList());
            result.Add(" ");
            result.AddRange(board2.Where(s => !string.IsNullOrEmpty(s)).ToList());
            return result;
        }

        protected abstract string GetBoardRowLine(Board board, int row, BoardDrawingParameters parameters, bool isOpponentBoard);

        
        protected abstract List<string> GetBoardHorizontalEnumeration(BoardDrawingParameters parameters);

        protected abstract string GetBoardHorizontalSeparator(BoardDrawingParameters parameters);

        protected string GetBoardHeader(string title, BoardDrawingParameters parameters)
        {
            
            var leftPadding = GetPadding(parameters.VerticalEnumeratorWidth);
            var rightPaddingLength = parameters.BoardWidth - title.Length - 1 > 0
                ? parameters.BoardWidth - title.Length - 1
                : 1; 
            var rightPadding = GetPadding(rightPaddingLength);
            return $"{leftPadding}{title}:{rightPadding}";
        }

        protected static string CellString(CellState cellState, bool isOpponentBoard)
        {
            switch (cellState)
            {
                case CellState.Empty: return "~";
                case CellState.Hit: return "H";
                case CellState.Miss: return "M";
                case CellState.Ship: return isOpponentBoard ? "~" : "S";
                case CellState.Error: return "E";
            }

            throw new Exception($"CellState {cellState} exists, but not processed here");
        }

        protected static string CenteredString(string s, int width) // https://stackoverflow.com/questions/18573004/how-to-center-align-arguments-in-a-format-string
        {
            if (s.Length >= width)
            {
                return s;
            }
            int leftPadding = (width - s.Length) / 2;
            int rightPadding = width - s.Length - leftPadding;

            return new string(' ', leftPadding) + s + new string(' ', rightPadding);
        }

        protected static string GetPadding(int lenght)
        {
            return new string(' ', lenght);
        }
    }
}