﻿using Domain.Enums;

namespace Domain
{
    public class ShipOnBoard
    {
        public int ShipOnBoardId { get; set; }

        public int ShipId { get; set; }
        public Ship Ship { get; set; } = null!;
        
        public int BoardId { get; set; }
        public Board Board { get; set; } = null!;
        
        public bool IsSunken { get; set; } = false;

        public int StartRow { get; set; }
        public int StartColumn { get; set; }

        public ELayout Layout { get; set; }
    }
}