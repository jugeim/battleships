﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    public class Board
    {
        public int BoardId { get; set; }
        
        public int Rows { get; set; }
        public int Columns { get; set; }

        public ICollection<ShipOnBoard> ShipsOnBoard { get; set; } = new List<ShipOnBoard>();

        public ICollection<Bomb> Bombs { get; set; } = new List<Bomb>();
        
       
    }
}