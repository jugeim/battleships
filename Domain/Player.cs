﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Enums;

namespace Domain
{
    public class Player
    {
        public int PlayerId { get; set; }

        public EPlayerType PlayerType { get; set; }

        [MaxLength(32)]
        public string Name { get; set; } = null!;

        public override string ToString()
        {
            return Name + " is " + PlayerType;
        }

        [ForeignKey(nameof(Board))]
        public int BoardId { get; set; }
        public Board Board  { get; set; } = null!;

    }
}