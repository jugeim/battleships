﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Enums;

namespace Domain
{
    public class Game
    {
        public int GameId { get; set; }

        [MaxLength(64)]
        public string Name { get; set; } = null!;

        public int GameOptionId { get; set; }
        public GameOption GameOption { get; set; } = null!;
        
        public int PlayerAId { get; set; }

        //[ForeignKey("PlayerAId")]
        public virtual Player PlayerA { get; set; } = new Player();

        public int PlayerBId { get; set; }

        //[ForeignKey("PlayerBId")]
        public virtual Player PlayerB { get; set; } = new Player();
        
        public bool NextMoveByA { get; set; }

        public EGamePhase GamePhase { get; set; } = EGamePhase.Gameplay;

    }
}
