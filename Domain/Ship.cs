﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using Microsoft.EntityFrameworkCore;

namespace Domain
{
    
    public class Ship
    {
        public int ShipId { get; set; }
       
        
        [Range(1, int.MaxValue)]
        public int Size { get; set; }

        [MaxLength(32)]
        public string Name { get; set; } = null!;

        public ICollection<GameOptionShip> GameOptionShips { get; set; } = null!;
        public ICollection<ShipOnBoard> ShipsOnBoard { get; set; } = null!;

        public override string ToString()
        {
            return $"{Name} (1x{Size})";
        }
    }
}