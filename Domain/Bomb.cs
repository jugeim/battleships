﻿namespace Domain
{
    public class Bomb
    {
        public int BombId { get; set; }
        
        public int BoardId { get; set; }
        public Board Board { get; set; } = null!;
        
        public int Row { get; set; }
        public int Column { get; set; }
        
        public int Order { get; set; }
    }
}