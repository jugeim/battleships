﻿﻿namespace Domain.Enums
{
    public enum ELayout
    {
        Horizontal,
        Vertical
    }
}