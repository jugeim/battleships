﻿namespace Domain.Enums
{
    //next move after successful hit
    public enum ENextMoveAfterHit
    {
        SamePlayer,
        OtherPlayer
    }
}