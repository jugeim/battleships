﻿using System.Text;

namespace Domain.Enums
{
    public enum EShipsCanTouch
    {
        No,
        Corner,
        Yes
    }
}