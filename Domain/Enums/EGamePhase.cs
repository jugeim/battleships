﻿namespace Domain.Enums
{
    public enum EGamePhase
    {
        Preparation,
        Gameplay
    }
}