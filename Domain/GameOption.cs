﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Enums;

namespace Domain
{
    public class GameOption
    {
        public int GameOptionId { get; set; }
        
        [MaxLength(64)]
        public string Name { get; set; } = null!;
 
        public int BoardWidth { get; set; }
        public int BoardHeight { get; set; }

        // enums are actually int in database
        public EShipsCanTouch ShipsCanTouch { get; set; } 

        public ENextMoveAfterHit NextMoveAfterHit { get; set; }
        
        public ICollection<GameOptionShip> GameOptionShips { get; set; } = null!;
        
        public ICollection<Game> Games { get; set; } = null!;
        
    }
}