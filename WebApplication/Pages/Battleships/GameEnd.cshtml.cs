﻿using System;
using System.Threading.Tasks;
using DAL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebApplication.Pages.Battleships
{
    public class GameEnd : PageModel
    {
        private readonly DAL.BattleshipsDbContext _context;

        public GameEnd(BattleshipsDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public int GameId { get; set; }
        public string Winner { get; set; } = default!;

        public void OnGet(int id, string winner)
        {
            GameId = id;
            Winner = winner;
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var game = await _context.Games.FindAsync(GameId);
            if (game == null)
            {
                Console.WriteLine("Better not get here");
                return Page();
            }

            _context.Entry(game).State = EntityState.Deleted;
            await _context.SaveChangesAsync();

            return RedirectToPage("../Index");
        }
    }
}