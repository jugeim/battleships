﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AI;
using Domain;
using Domain.Enums;
using GameBrain;
using GameBrain.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Board = Domain.Board;
using Player = GameBrain.Player;

namespace WebApplication.Pages.Battleships
{
    public class Gameplay : PageModel
    {

        private readonly DAL.BattleshipsDbContext _context;
        
        public Game? Game { get; set; }
        public GameBrain.Battleships GameBrain { get; set; } = new();

        public bool IsAiTurn { get; set; } = false;
        public bool ShouldDisableButton { get; set; } = true;
        
        [BindProperty]
        public int GameId { get; set; }

        [BindProperty]
        [Range(0, 1000)]
        public int? Row { get; set; }
        
        [BindProperty]
        [Range(0, 1000)]
        public int? Column { get; set; }

        public int Rows => GameBrain.BoardDimensions.Height;
        
        public int Columns => GameBrain.BoardDimensions.Width;



        public Gameplay(DAL.BattleshipsDbContext context)
        {
            _context = context;
        }
        
        public async Task<IActionResult> OnGetAsync(int id)
        {
            GameId = id;
            await LoadGameFromDb(id);

            GameBrain.SetGameStateFromDbData(Game!);

            var winner = GameBrain.GetWinner();
            if (winner != null)
            {
                return RedirectToPage("./GameEnd", new {id = GameId, winner = winner.Name});
            }

            var player = GameBrain.NextMoveByA ? GameBrain.PlayerA : GameBrain.PlayerB;
            if (player.PlayerType == EPlayerType.AI)
            {
                IsAiTurn = true;
                ShouldDisableButton = false;
                
                var (row, col) = AiGiveMoveCoordinates();
                
                ViewData["initCoords"] = $"({row}, {col})";
                Row = row;
                Column = col;
            }
            else
            {
                ViewData["initCoords"] = "(row, col)";
                Row = Column = -1;
            }
            
            return Page();
        }

        private (int row, int col) AiGiveMoveCoordinates()
        {
            var opponent = GameBrain.NextMoveByA ? GameBrain.PlayerB : GameBrain.PlayerA;

            var aiEngine = new RegularAi(GameBrain.BoardDimensions.Height, GameBrain.BoardDimensions.Width,
                GameBrain.ShipsCanTouch, GetShipSizeShipAmountDictionary());
            aiEngine.SetAiKnowledgeByPlacedBombs(ComputeMovesImpact(opponent.Board));
            return aiEngine.GiveMoveCoordinates();
            
        }

        private Dictionary<(int row, int col), bool> ComputeMovesImpact(GameBrain.Board opponentsBoard)
        {
            var res = new Dictionary<(int, int), bool>();
            var shipCellSet = new HashSet<Position>(opponentsBoard.AllShipCells());
            foreach (var position in opponentsBoard.BombCells)
            {
                
                res.Add(position.GetCoordinates(), shipCellSet.Contains(position));
            }

            return res;
        }

        private Dictionary<int, int> GetShipSizeShipAmountDictionary()
        {
            return GameBrain.GameSessionShips
                .Select(pair => (pair.Key.Size, pair.Value))
                .GroupBy(
                    pair => pair.Size,
                    pair => pair.Value,
                    (size, values) => new {Key = size, Count = values.Sum()}
                )
                .ToDictionary(pair => pair.Key, pair => pair.Count);
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return RedirectToPage(new {id = GameId});
            }
            await LoadGameFromDb(GameId);
            if (Game == null) return RedirectToPage("../Index");
            GameBrain.SetGameStateFromDbData(Game);
            try
            {
                var hasHit = GameBrain.PlaceBomb(Row!.Value, Column!.Value);
                await InsertNewBombIntoDb();
                if (GameBrain.NextMoveAfterHit == ENextMoveAfterHit.OtherPlayer || !hasHit)
                {
                    Game.NextMoveByA = !Game.NextMoveByA;
                    await _context.SaveChangesAsync();
                }

                if (hasHit)
                {
                    var board = GameBrain.NextMoveByA ? GameBrain.PlayerB.Board : GameBrain.PlayerA.Board;
                    await UpdateSunkenShips(board);
                }
                return RedirectToPage("./Index", new {id = GameId});
            }
            catch (GameEndException)
            {
                var winner = GameBrain.NextMoveByA ? GameBrain.PlayerA.Name : GameBrain.PlayerB.Name;
                return RedirectToPage("./GameEnd", new {id = GameId, winner});
            }
            catch (ArgumentException)
            {
                throw new ArgumentException(
                    "Please, don't use Postman or something, there is only a js validation for bombs on page");
            }
            
        }

        private async Task UpdateSunkenShips(GameBrain.Board board)
        {
            foreach (var ship in board.Ships)
            {
                var dbShip = await _context.ShipsOnBoard
                    .Where(x => x.BoardId == board.BoardId && x.ShipId == ship.ShipId
                                                           && x.StartRow == ship.AnchorPosition!.Row
                                                           && x.StartColumn == ship.AnchorPosition!.Column)
                    .FirstOrDefaultAsync();
                dbShip.IsSunken = ship.IsSunken;
            }
            await _context.SaveChangesAsync();
        }

        private async Task InsertNewBombIntoDb()
        {
            if (Game == null || Row == null || Column == null) throw new Exception("Programmer error");
            
            var opponentBoard = Game.NextMoveByA ? Game.PlayerB.Board : Game.PlayerA.Board;
            var order = opponentBoard.Bombs.Count;

            var placed = new Bomb
            {
                BoardId = opponentBoard.BoardId,
                Order = order,
                Row = Row.Value,
                Column = Column.Value,
            };

            _context.Bombs.Add(placed);
            await _context.SaveChangesAsync();
        }

        /*
         * Changed long Include sequence to separate loads => became ~1000 times faster 
         */
        private async Task LoadGameFromDb(int id)
        {
            Game = await _context.Games.FindAsync(id);
            
            await _context.GameOptions.Where(o => o.GameOptionId == Game.GameOptionId).LoadAsync();
            
            await _context.GameOptionShips
                    .Where(x => x.GameOptionId == Game.GameOptionId).LoadAsync();
            var gameOptShips = await _context.GameOptionShips
                .Where(x => x.GameOptionId == Game.GameOptionId).ToListAsync();
            
            foreach (var s in gameOptShips)
            {
                await _context.Ships.FindAsync(s.ShipId);
            }

            await _context.Players.FindAsync(Game.PlayerAId);
            await _context.Players.FindAsync(Game.PlayerBId);
            var b1= await _context.Boards.FindAsync(Game.PlayerA.BoardId);
            var b2 = await _context.Boards.FindAsync(Game.PlayerB.BoardId);
                
            await _context.Bombs.Where(x => x.BoardId == b1.BoardId).LoadAsync();
            await _context.Bombs.Where(x => x.BoardId == b2.BoardId).LoadAsync();
                
            await _context.ShipsOnBoard.Where(x => x.BoardId == b1.BoardId).LoadAsync();
            await _context.ShipsOnBoard.Where(x => x.BoardId == b2.BoardId).LoadAsync();
        }
    }
}