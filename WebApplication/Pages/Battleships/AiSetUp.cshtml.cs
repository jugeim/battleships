﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DAL;
using Domain;
using Domain.Enums;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Player = GameBrain.Player;
using Ship = GameBrain.Ship;

namespace WebApplication.Pages.Battleships
{
    public class AiSetUp : PageModel
    {
        private readonly DAL.BattleshipsDbContext _context;

        public AiSetUp(BattleshipsDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> OnGetAsync(int gid, int pid)
        {
            if (gid < 0 || pid < 0) return RedirectToPage(".../Index");
            var (game, player) = await LoadGameFromDbPartially(gid, pid);

            var placedShips = PlaceShipsOnBoardUsingGameBrain(game);

            foreach (var ship in placedShips)
            {
                _context.ShipsOnBoard.Add(new ShipOnBoard
                {
                    BoardId = player.BoardId,
                    ShipId = ship.ShipId,
                    StartRow = ship.AnchorPosition!.Row,
                    StartColumn = ship.AnchorPosition!.Column,
                    Layout = ship.Layout
                });
            }

            game.NextMoveByA = !game.NextMoveByA;
            if (game.NextMoveByA) game.GamePhase = EGamePhase.Gameplay;
            
            await _context.SaveChangesAsync();
            ViewData["id"] = gid;
            return Page();
        }
        
        private async Task<(Game, Domain.Player)> LoadGameFromDbPartially(int gid, int pid)
        {
            Game game = await _context.Games.FindAsync(gid);
            
            await _context.GameOptions.Where(o => o.GameOptionId == game.GameOptionId).LoadAsync();
            
            await _context.GameOptionShips
                .Where(x => x.GameOptionId == game.GameOptionId).LoadAsync();
            var gameOptShips = await _context.GameOptionShips
                .Where(x => x.GameOptionId == game.GameOptionId).ToListAsync();
            
            foreach (var s in gameOptShips)
            {
                await _context.Ships.FindAsync(s.ShipId);
            }

            if (pid != game.PlayerAId && pid != game.PlayerBId) throw new Exception("Invalid parameters on get"); 

            var player = await _context.Players.FindAsync(pid);
            var b= await _context.Boards.FindAsync(player.BoardId);
            
            await _context.ShipsOnBoard.Where(x => x.BoardId == b.BoardId).LoadAsync();

            return (game, player);
        }

        private IEnumerable<Ship> PlaceShipsOnBoardUsingGameBrain(Game game)
        {
            var battleships = new GameBrain.Battleships();
            battleships.SetGameStateFromDbData(game);
            var currPlayer = game.NextMoveByA ? battleships.PlayerA : battleships.PlayerB;
            
            var readyShips = currPlayer.Board.Ships;
            var gameShips = battleships.GameSessionShips
                .SelectMany(pair => Enumerable.Repeat(pair.Key, pair.Value).ToList()).ToList();
            foreach (var ship in readyShips)
            {
                gameShips.Remove(ship);
            }

            var shipsQueue = new Queue<Ship>(gameShips);
            Console.WriteLine("Queue init size " + shipsQueue.Count);

            var cycle = 0;
            while (shipsQueue.Count != 0 && cycle < 100)
            {
                try
                {
                    var ship = shipsQueue.Dequeue();
                    PlaceShipRandomly(battleships, currPlayer, ship);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    currPlayer.Board.Ships.ForEach(x => shipsQueue.Enqueue(x));
                    currPlayer.Board.Ships.Clear();
                    cycle++;
                }
            }

            if (cycle == 100)
            {
                throw new Exception("Random failed");
                
            }
            return currPlayer.Board.Ships;
        }
        
        private void PlaceShipRandomly(GameBrain.Battleships game, Player player, Ship ship)
        {
            var board = player.Board;

            var rand = new Random();
            var rowPool = Enumerable.Range(0, board.Rows).OrderBy(x => rand.Next()).ToList();
            var colPool = Enumerable.Range(0, board.Columns).ToList();

            foreach (var randRow in rowPool)
            {
                foreach (var randCol in colPool.OrderBy(x => rand.Next()).ToList())
                {
                    var randLayout = (ELayout) rand.Next(2);
                    var randShip = new Ship(ship, randRow, randCol, randLayout);
                    var canShipBePlaced = board.CanShipBePlaced(randShip, false, game.ShipsCanTouch);
                    if (!canShipBePlaced)
                    {
                        randShip.SwitchLayout();
                        canShipBePlaced = board.CanShipBePlaced(randShip, false, game.ShipsCanTouch);
                    }
                    if (canShipBePlaced)
                    {
                        board.PlaceShip(randShip);
                        return;
                    }
                }
            }
            throw new Exception("Can not place ship");
        }
    }
}