﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using Domain.Enums;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebApplication.Pages.Battleships
{
    public class Index : PageModel
    {
        private readonly DAL.BattleshipsDbContext _context;
        
        public Index(BattleshipsDbContext context)
        {
            _context = context;
        }

        public string PlayerName { get; set; } = "Yet Unknown";
        public string BtnText { get; set; } = "Undefined";
        public string Handler { get; set; } = "none";

        public async Task OnGetAsync(int id)
        {
            var game = await _context.Games.FindAsync(id);
            var player = await _context.Players.FindAsync(game.NextMoveByA ? game.PlayerAId : game.PlayerBId);

            PlayerName = player.Name;
            if (game.GamePhase == EGamePhase.Preparation)
            {
                if (player.PlayerType == EPlayerType.Human)
                {
                    BtnText = "Begin (Continue) Ship Placement";
                    Handler = "Setup";
                }
                else
                {
                    BtnText = $"Click to place {PlayerName}'s ships, please";
                    Handler = "AI";
                    ViewData["pid"] = player.PlayerId;
                }
            }
            else
            {
                BtnText = "Attack!";
                Handler = "Gameplay";
            }
            ViewData["id"] = id;
        }

        public IActionResult OnPost(int id, int pid, string handler)
        {
            if (handler == "AI") return OnPostAI(id, pid);
            if (handler == "Setup") return OnPostSetup(id);
            if (handler == "Gameplay") return OnPostGameplay(id);
            throw new Exception("unknown handler " + handler);
        }

        private IActionResult OnPostAI(int id, int pid)
        {
            return RedirectToPage("./AiSetUp", new {gid = id, pid});
        }

        private IActionResult OnPostSetup(int id)
        {
            return RedirectToPage("./SetUp", new {id});
        }

        private IActionResult OnPostGameplay(int id)
        {
            return RedirectToPage("./Gameplay", new {id});
        }
    }
}