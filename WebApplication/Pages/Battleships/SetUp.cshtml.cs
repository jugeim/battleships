﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DAL;
using Domain;
using Domain.Enums;
using GameBrain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Player = Domain.Player;
using Ship = GameBrain.Ship;

namespace WebApplication.Pages.Battleships
{
    public class SetUp : PageModel
    {
        private readonly DAL.BattleshipsDbContext _context;
        
        public SetUp(BattleshipsDbContext context)
        {
            _context = context;
        }

        public Game? Game { get; set; }
        public GameBrain.Battleships GameBrain { get; set; } = new GameBrain.Battleships();
        public GameBrain.Player CurrentPlayer { get; set; } = new GameBrain.Player();

        public List<Ship> AvailableShips { get; set; } = new List<Ship>();
        public List<SelectListItem> ShipsSelectList { get; set; } = new();
        public string FirstSelectedShipSize => ShipsSelectList.Count > 0 ? 
            Regex.Match(ShipsSelectList.First().Text, "1x(\\d+)").Groups[1].Value : "0";

        public int Rows => GameBrain.BoardDimensions.Height;
        public int Columns => GameBrain.BoardDimensions.Width;

        public int ShipPlacementRule => (int) GameBrain.ShipsCanTouch;
        
        [BindProperty]
        [Range(1, int.MaxValue)]
        public int GameId { get; set; }

        [BindProperty]
        [Range(1, int.MaxValue)]
        public int? ShipId { get; set; }

        [BindProperty]
        [Range(0, 1000)]
        public int? AnchorX { get; set; }
        
        [BindProperty]
        [Range(0, 1000)]
        public int? AnchorY { get; set; }
        
        [BindProperty]
        public bool IsVerticalLayout { get; set; }

        [BindProperty] public bool WasPlaced { get; set; } = false;
        
        public async Task<IActionResult> OnGetAsync(int id)
        {
            if (id < 1) return Page();
            await LoadGameFromDbAsync(id);
            GameId = id;
            GameBrain.SetGameStateFromDbData(Game!);
            CurrentPlayer = GameBrain.NextMoveByA ? GameBrain.PlayerA : GameBrain.PlayerB;
            
            var readyShips = CurrentPlayer.Board.Ships;
            var gameShips = GameBrain.GameSessionShips
                .SelectMany(pair => Enumerable.Repeat(pair.Key, pair.Value).ToList()).ToList();

            foreach (var ship in readyShips)
            {
                gameShips.Remove(ship);
            }

            if (gameShips.Count == 0 && Game != null)
            {
                Game.NextMoveByA = !Game.NextMoveByA;
                if (Game.NextMoveByA)  Game.GamePhase = EGamePhase.Gameplay;
                
                await _context.SaveChangesAsync();
                return RedirectToPage("./Index", new {id});
            }

            AvailableShips = gameShips;
            var selectList = new List<SelectListItem>();
            var firstSelected = true;
            foreach (var ship in gameShips)
            {
                selectList.Add(new SelectListItem(ship.ToString(), ship.ShipId.ToString(), firstSelected));
                firstSelected = false;
            }
            ShipsSelectList = selectList;
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                Console.WriteLine("Something is invalid");
                return RedirectToPage(new {id = GameId});
            }

            Game = await _context.Games.FindAsync(GameId);
            await _context.GameOptions.FindAsync(Game.GameOptionId);
            await _context.GameOptionShips.Where(x => x.GameOptionId == Game.GameOptionId)
                .LoadAsync();
            
            var currentPlayer = await _context.Players
                .FindAsync(Game.NextMoveByA ? Game.PlayerAId : Game.PlayerBId);

            var placed = new ShipOnBoard()
            {
                ShipId = ShipId!.Value,
                BoardId = currentPlayer.BoardId,
                StartRow = AnchorY!.Value,
                StartColumn = AnchorX!.Value,
                Layout = IsVerticalLayout ? ELayout.Vertical : ELayout.Horizontal
            };
            
            var optionShipsCount = Game.GameOption.GameOptionShips
                .SelectMany(x => Enumerable.Repeat(0, x.Amount)).ToList().Count;
            var playerShips = await _context.Boards
                .Where(x => x.BoardId == currentPlayer.BoardId)
                .Include(x => x.ShipsOnBoard).ToListAsync();
            var playerShipsCount = playerShips.Count;

            string path = "./SetUp";
            if (playerShipsCount == optionShipsCount && !Game.NextMoveByA)
            {
                Game.GamePhase = EGamePhase.Gameplay;
                path = "./Index";
            } else if (playerShipsCount == optionShipsCount && Game.NextMoveByA)
            {
                Game.NextMoveByA = !Game.NextMoveByA;
                path = "./Index";

            } 

            _context.ShipsOnBoard.Add(placed);
            await _context.SaveChangesAsync();

            return RedirectToPage(path, new {id = GameId});
            
        }

        private async Task LoadGameFromDbAsync(int id)
        {
            Game = await _context.Games.FindAsync(id);
            
            await _context.GameOptions.Where(o => o.GameOptionId == Game.GameOptionId).LoadAsync();
            
            await _context.GameOptionShips
                .Where(x => x.GameOptionId == Game.GameOptionId).LoadAsync();
            var gameOptShips = await _context.GameOptionShips
                .Where(x => x.GameOptionId == Game.GameOptionId).ToListAsync();
            
            foreach (var s in gameOptShips)
            {
                await _context.Ships.FindAsync(s.ShipId);
            }

            await _context.Players.FindAsync(Game.PlayerAId);
            await _context.Players.FindAsync(Game.PlayerBId);
            var b1= await _context.Boards.FindAsync(Game.PlayerA.BoardId);
            var b2 = await _context.Boards.FindAsync(Game.PlayerB.BoardId);
                
            await _context.Bombs.Where(x => x.BoardId == b1.BoardId).LoadAsync();
            await _context.Bombs.Where(x => x.BoardId == b2.BoardId).LoadAsync();
                
            await _context.ShipsOnBoard.Where(x => x.BoardId == b1.BoardId).LoadAsync();
            await _context.ShipsOnBoard.Where(x => x.BoardId == b2.BoardId).LoadAsync();
        }
    }
}