using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using DAL;
using Domain;
using Domain.Enums;
using GameBrain;
using Board = Domain.Board;
using Player = Domain.Player;

namespace WebApplication.Pages.Games
{
    public class CreateModel : PageModel
    {
        private readonly DAL.BattleshipsDbContext _context;


        [BindProperty]
        [Required, MinLength(1), MaxLength (21)]
        public string GameName { get; set; } = null!;
        
        [BindProperty]
        [Required, MinLength(1), MaxLength (21)]
        public string PlayerAName { get; set; } = null!;

        [BindProperty]
        public EPlayerType PlayerAType { get; set; }
        
        [BindProperty]
        [Required, MinLength(1), MaxLength (21)]
        public string PlayerBName { get; set; } = null!;
        
        [BindProperty]
        public EPlayerType PlayerBType { get; set; }
        
        [BindProperty]
        [Required]
        public int GameOptionId { get; set; }

        public SelectList DbGameOptions { get; set; } = new(new List<object>());

        public CreateModel(DAL.BattleshipsDbContext context)
        {
            _context = context;
            
        }

        public IActionResult OnGet()
        {
            DbGameOptions = new SelectList(_context.GameOptions, 
                nameof(GameOption.GameOptionId), 
                nameof(GameOption.Name));
            return Page();
        }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            
            if (!ModelState.IsValid)
            {
                DbGameOptions = new SelectList(_context.GameOptions, 
                    nameof(GameOption.GameOptionId), 
                    nameof(GameOption.Name));

                return Page();
            }
            
            Console.WriteLine(PlayerAType);
            Console.WriteLine(PlayerBType);
            
            var options = await _context.GameOptions.FindAsync(GameOptionId);

            var game = new Game();
            Console.WriteLine(PlayerAName);
            Console.WriteLine(PlayerBName);
            game.Name = GameName;
            Console.WriteLine("Game Name: " + game.Name);

            Player playerA = new()
            {
                PlayerType = PlayerAType,
                Name =  PlayerAName,
                Board = new Board
                {
                    Rows = options.BoardHeight,
                    Columns =  options.BoardWidth
                }
            };
            
            Player playerB = new()
            {
                PlayerType = PlayerBType,
                Name = PlayerBName,
                Board = new Board{
                    Rows = options.BoardHeight,
                    Columns =  options.BoardWidth
                }
            };

            game.PlayerA = playerA;
            game.PlayerB = playerB;
            game.Name = GameName;
            game.NextMoveByA = true;
            game.GameOptionId = GameOptionId;
            game.GamePhase = EGamePhase.Preparation;

            _context.Games.Add(game);
            await _context.SaveChangesAsync();

            return RedirectToPage("../Battleships/Index", new {id = game.GameId});
        }
    }
}
