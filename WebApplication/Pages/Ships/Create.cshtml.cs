﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebApplication.Pages.Ships
{
    public class CreateModel : PageModel
    {
        private readonly DAL.BattleshipsDbContext _context;
        
        
        [BindProperty]
        [Required]
        [MinLength(1)] 
        public string? Name { get; set; }
        
        [BindProperty] public int Size { get; set; }

        [BindProperty] public bool ShouldReturnToOptionCreation { get; set; }

        public CreateModel(DAL.BattleshipsDbContext context)
        {
            _context = context;
            
        }

        public void OnGet(string? from)
        {
            if (from == "optCreate") ShouldReturnToOptionCreation = true;
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var s = new Ship
            {
                Name = Name!,
                Size = Size
            };
            _context.Add(s);
            await _context.SaveChangesAsync();

            if (ShouldReturnToOptionCreation)
            {
                return Redirect("../Options/Create");
            }
            
            return Redirect("./Index");
        }
    }
}