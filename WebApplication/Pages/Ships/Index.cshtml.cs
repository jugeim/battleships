﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebApplication.Pages.Ships
{
    public class Index : PageModel
    {
        private readonly DAL.BattleshipsDbContext _context;

        public List<Ship> DbShips { get; set; } = new();

        
        public Index(DAL.BattleshipsDbContext context)
        {
            _context = context;
            
        }
        
        public async Task OnGetAsync()
        {
            DbShips = await _context.Ships.ToListAsync();
        }
    }
}