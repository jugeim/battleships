﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace WebApplication.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly DAL.BattleshipsDbContext _context;

        public IndexModel(DAL.BattleshipsDbContext context, ILogger<IndexModel> logger)
        {
            _logger = logger;
            _context = context;

        }

        public IList<Game> Game { get; set; } = null!;

        public async Task OnGetAsync()
        {
            Game = await _context.Games.OrderByDescending(x => x.GameId).ToListAsync();
            await _context.GameOptions.LoadAsync();
            await _context.Players.LoadAsync();
        }

    }
}