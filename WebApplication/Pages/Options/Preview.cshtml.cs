﻿using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebApplication.Pages.Options
{
    public class Preview : PageModel
    {

        private readonly DAL.BattleshipsDbContext _context;
        
        public Preview(DAL.BattleshipsDbContext context)
        {
            _context = context;
            
        }
        
        public GameOption SelectedGameOption { get; set; } = default!;
        
        public async Task OnGetAsync(int id)
        {
            SelectedGameOption = await _context.GameOptions
                .Where(x => x.GameOptionId == id)
                .Include(x => x.GameOptionShips)
                .ThenInclude(s => s.Ship)
                .FirstOrDefaultAsync();
            ViewData["GameOption"] = SelectedGameOption;
        }
    }
}