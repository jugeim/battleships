using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;

namespace WebApplication.Pages.Options
{
    public class DeleteModel : PageModel
    {
        private readonly DAL.BattleshipsDbContext _context;

        public DeleteModel(DAL.BattleshipsDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public GameOption GameOption { get; set; } = null!;

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            GameOption = await _context.GameOptions.FirstOrDefaultAsync(m => m.GameOptionId == id);

            if (GameOption == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            GameOption = await _context.GameOptions.FindAsync(id);

            if (GameOption != null)
            {
                _context.GameOptions.Remove(GameOption);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
