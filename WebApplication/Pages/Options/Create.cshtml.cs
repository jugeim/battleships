﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Domain.Enums;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebApplication.Pages.Options
{
    public class CreateModel : PageModel
    {
        
        private readonly DAL.BattleshipsDbContext _context;
        
        [BindProperty] public string Name { get; set; } = string.Empty;

        [BindProperty] public int BoardHeight { get; set; }
        [BindProperty] public int BoardWidth { get; set; }
        
        [BindProperty] public EShipsCanTouch ShipsCanTouch { get; set; }
        [BindProperty] public bool IsSamePlayerAfterHit { get; set; }

        [BindProperty] public Dictionary<int, int> OptionShips { get; set; } = new();

        public List<Ship> DbShips { get; set; } = new();
        
        
        
        public CreateModel(DAL.BattleshipsDbContext context)
        {
            _context = context;
            
        }

        private async Task FillDbShipsWithDefaults()
        {
            DbShips = await _context.Ships.ToListAsync();
            foreach (var ship in DbShips)
            {
                OptionShips.Add(ship.ShipId, 0);
            }
        }
        
        public async Task OnGetAsync()
        {
            await FillDbShipsWithDefaults();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            Console.WriteLine(BoardHeight);
            Console.WriteLine(BoardWidth);
            Console.WriteLine(ShipsCanTouch);
            Console.WriteLine(IsSamePlayerAfterHit);
            if (!ModelState.IsValid)
            {
                await FillDbShipsWithDefaults();
                return Page();
            }

            var option = new GameOption
            {
                Name = Name, 
                BoardHeight = BoardHeight, 
                BoardWidth = BoardWidth, 
                ShipsCanTouch = ShipsCanTouch,
                NextMoveAfterHit = IsSamePlayerAfterHit
                    ? ENextMoveAfterHit.SamePlayer
                    : ENextMoveAfterHit.OtherPlayer,
                GameOptionShips = new List<GameOptionShip>()
            };
            foreach (var (shipId, amount) in OptionShips)
            {
                if (amount < 1) continue;
                option.GameOptionShips.Add(new GameOptionShip
                {
                    Amount = amount,
                    ShipId = shipId
                });
            }

            _context.Add(option);
            await _context.SaveChangesAsync();
            
            return RedirectToPage("../Games/Create");
        }
    }
}