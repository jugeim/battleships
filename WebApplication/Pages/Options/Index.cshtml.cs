using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;

namespace WebApplication.Pages.Options
{
    public class IndexModel : PageModel
    {
        private readonly DAL.BattleshipsDbContext _context;

        public IndexModel(DAL.BattleshipsDbContext context)
        {
            _context = context;
        }

        public IList<GameOption> GameOption { get;set; } = null!;

        public async Task OnGetAsync()
        {
            GameOption = await _context.GameOptions.ToListAsync();
        }
    }
}
