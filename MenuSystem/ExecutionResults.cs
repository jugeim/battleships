﻿namespace MenuSystem
{
    public class ExecutionResults
    {
        public NavigationCommand Command { get; set; }
        public string? StrValue { get; set; }
        public int? IntValue { get; set; } // probably someday
        public bool ShouldLoadGame { get; set; }

        public ExecutionResults WithCommand(NavigationCommand command)
        {
            Command = command;
            return this;
        }
    }
}