﻿using System;
using System.Linq;

namespace MenuSystem.MenuItems
{
    public class MenuItem
    {
        public virtual string UserChoice { get; set; }
        public virtual string Label { get; set; }
        
        public virtual Func<ExecutionResults> MethodToExecute { get; set; }

        public MenuItem(string userChoice, string label, Func<ExecutionResults> methodToExecute)
        {
            if (userChoice.Length + label.Length > 98)
            {
                throw new Exception("Do not make menu items that long. " +
                                    "Maximum item length is 100 - 2 (separator \") \" length)");
            }
            UserChoice = userChoice.ToLower();
            Label = label;
            MethodToExecute = methodToExecute;
        }
 
        public  MenuItem(string userChoice, string label) : this(userChoice, label, DefaultMenuItemFunc)
        {
        }
        
        protected static ExecutionResults DefaultMenuItemFunc()
        {
            Console.Clear();
            Console.WriteLine("==========> Menu <==========");
            Console.WriteLine("Not implemented yet");
            
            System.Threading.Thread.Sleep(1000);

            return new ExecutionResults {Command = NavigationCommand.Stay};
        }

        public override string ToString()
        {
            return UserChoice.First().ToString().ToUpper() + UserChoice.Substring(1) + ") " + Label;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is MenuItem))
            {
                return false;
            }
            var other = (MenuItem) obj;
            return UserChoice == other.UserChoice;
        }

        public override int GetHashCode()
        {
            return UserChoice.GetHashCode();
        }
    }
}