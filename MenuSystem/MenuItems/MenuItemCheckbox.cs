﻿﻿using System;

namespace MenuSystem.MenuItems
{
    public class MenuItemCheckbox : MenuItem
    {
        private bool _isSelected;

        private readonly Func<ExecutionResults> _mainMethodToExecute = () => new ExecutionResults {Command = NavigationCommand.Stay};
        
        public MenuItemCheckbox(string userChoice, string label) : base(userChoice, label)
        {
            MethodToExecute = MethodSwitch;
        }
        
        public MenuItemCheckbox(string userChoice, string label, bool isSelected) : base(userChoice, label)
        {
            _isSelected = isSelected;
            MethodToExecute = MethodSwitch;
        }
        
        public MenuItemCheckbox(string userChoice, string label, Func<ExecutionResults> methodToExecute) : base(userChoice, label, methodToExecute)
        {
            _mainMethodToExecute = methodToExecute;
            MethodToExecute = MethodSwitch;
        }

        private ExecutionResults MethodSwitch()
        {
            _isSelected = !_isSelected;
            return _mainMethodToExecute.Invoke();
        }

        public bool IsSelected()
        {
            return _isSelected;
        }

        public override string ToString()
        {
            var selection = _isSelected ? "v" : "x";
            return UserChoice.ToUpper() + ") " + Label + " [" + selection + "]";
        }
    }
}