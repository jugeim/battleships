﻿using System;
using System.IO;
using GameBrain;

namespace MenuSystem.MenuItems
{
    public class MenuItemFileLoader : MenuItem
    {
        public  readonly Battleships Game;
        
        public MenuItemFileLoader(string userChoice, string label, Battleships game) : base(userChoice, label)
        {
            MethodToExecute = LoadSaveFile;
            Game = game;
        }

        private ExecutionResults LoadSaveFile()
        {
            var jsonString = File.ReadAllText(Label);

            Game.SetGameStateFormJsonString(jsonString);

            return new ExecutionResults
            {
                Command = NavigationCommand.ToPrevious,
                ShouldLoadGame = true
            };
        }
        
    }
}