﻿using System;
using System.Linq;

namespace MenuSystem.MenuItems
{
    // MethodToExecute access externalData and change it. To reflect changes MenuItem MethodToExecute now returns ExecutionResults
    public class MenuItemReflector : MenuItem
    {
        public string Value { get; set; }
        
        private readonly Func<ExecutionResults> _methodToExecute;
        
        public MenuItemReflector(string userChoice, string label, string initialValue, Func<ExecutionResults> methodToExecute)
            : base(userChoice, label, methodToExecute)
        {
            Value = initialValue;
            _methodToExecute = methodToExecute;
            MethodToExecute = ExecuteAndUpdate;
        }

        private ExecutionResults ExecuteAndUpdate()
        {
            var results = _methodToExecute.Invoke();
            Value = results.StrValue ?? "";

            return results;
        }

        public override string ToString()
        {
            return UserChoice.First().ToString().ToUpper() + UserChoice.Substring(1) + ") " + Label 
                   + $"{(Value.Trim() == "" ? "" : ": ")}" + Value;
        }
    }
}