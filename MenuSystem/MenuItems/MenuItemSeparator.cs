﻿﻿using System;
using System.Text;

namespace MenuSystem.MenuItems
{
    
    
    public class MenuItemSeparator : MenuItem
    {

        private int MaxLength; 
         
        private MenuItemSeparator(string userChoice, string label, Func<ExecutionResults> methodToExec) : base(userChoice, label, methodToExec)
        {
        }
        
        public MenuItemSeparator(string separationString, int length = 20) : this(separationString, "", UnreachableMethod)
        {
            MaxLength = length;
        }

        private static ExecutionResults UnreachableMethod()
        {
            return new ExecutionResults();
        }

        public override string ToString()
        {
            var result = new StringBuilder("");
            while (result.Length < MaxLength)
            {
                result.Append(UserChoice);

            }
            return result.ToString().Substring(0, MaxLength);
        }
    }
}