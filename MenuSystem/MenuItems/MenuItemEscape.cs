﻿﻿using System;

namespace MenuSystem.MenuItems
{
    public class MenuItemEscape : MenuItem
    {
        public MenuItemEscape(string userChoice, string label, NavigationCommand command) : base(userChoice, label)
        {
            MethodToExecute = () => new ExecutionResults {Command = command};
        }

        public MenuItemEscape(NavigationCommand navigationCommand) 
            : this(GetUserChoiceAndLabel(navigationCommand).userChoice, GetUserChoiceAndLabel(navigationCommand).label, navigationCommand)
        {
            MethodToExecute = () => new ExecutionResults {Command = navigationCommand};
        }

        private static (string userChoice, string label) GetUserChoiceAndLabel(NavigationCommand navigationCommand)
        {
            return navigationCommand switch
            {
                NavigationCommand.Exit => ("x", "eXit"),
                NavigationCommand.ToMain => ("m", "Return to Main"),
                NavigationCommand.ToPrevious => ("r", "Return"),
                _ => throw new ArgumentException($"{navigationCommand} should not be passed to this method.")
            };
        }
    }
}