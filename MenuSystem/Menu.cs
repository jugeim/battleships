﻿using System;
using System.Collections.Generic;
using System.Linq;
using MenuSystem.MenuItems;
using MenuSystem.View;

namespace MenuSystem
{
    public enum MenuLevel
    {
        Level0,
        Level1,
        Level2Plus
    }
    
    // Menu business logic
    // Navigation can be done by arrow-Enter control (left and right arrows disabled) or by textInput-Enter control
    // (no additional setting required).
    // To be correctly drawn, Menu has keep all its items (witch include separators, which can practically be same)
    // so all menu items are kept in list, and MenuItems dictionary contains only indexes of elements of that list.
    public class Menu
    {
        public string Title { get; set; } = "Menu";
        public MenuView View { get; set; } = new MenuView();
        public List<MenuItem> MenuItems { get; private set; } = new List<MenuItem>();
        private readonly MenuLevel _menuLevel;
        private List<string> DefaultMenuItems = new List<string>(); // keep in order: last in menu list => first here

        private bool _shouldRemoveLastMenuItem = false;

        public Menu(MenuLevel level)
        {
            _menuLevel = level;
            AddDefaultMenuItems();
        }

        public Menu(MenuLevel level, string title) : this(level)
        {
            Title = title;
        }

        public Menu(MenuLevel level, MenuView view) : this(level)
        {
            View = view;
        }

        public Menu(MenuLevel level, string title, MenuView view) : this(level, title)
        {
            View = view;
        }

        private int GetDefaultChoicesCount() // one default separator and amount of default choices (exit ,toMain, return)
        {
            return _menuLevel switch
            {
                MenuLevel.Level0 => 2,
                MenuLevel.Level1 => 3,
                _ => 4
            };
        }

        public void AddMenuItem(MenuItem item)
        {
            if (!MenuItems.Contains(item))
            { 
                MenuItems.Insert(MenuItems.Count - DefaultMenuItems.Count - 1, item);
            }
            else if (item.GetType() != typeof(MenuItemSeparator))
            {
                throw new ArgumentException($"User Choice {item.UserChoice} is occupied.");
            }
        }
        
        public void InsertMenuItemBeforeGivenUserChoice(MenuItem newItem, string nextMenuItemChoice)
        {
            var index = MenuItems.FindIndex(item => item.UserChoice == nextMenuItemChoice);

            if (index >= 0) MenuItems.Insert(index, newItem);
        }

        private void AddDefaultMenuItems()
        {
            MenuItem item = new MenuItemSeparator("=", Title.Length + 24);
            MenuItems.Add(item);
            
            if (_menuLevel == MenuLevel.Level2Plus)
            {
                item = new MenuItemEscape(NavigationCommand.ToPrevious);
                MenuItems.Add(item);
                DefaultMenuItems.Insert(0, "r");
            }
            if (_menuLevel >= MenuLevel.Level1)
            {
                item = new MenuItemEscape(NavigationCommand.ToMain);
                MenuItems.Add(item);
                
                DefaultMenuItems.Insert(0, "m");
            }
            item = new MenuItemEscape(NavigationCommand.Exit);
            MenuItems.Add(item);
            DefaultMenuItems.Insert(0, "x");
        }

        public void RemoveDefaultMenuItems()
        {
            DefaultMenuItems.Clear();
            MenuItems = MenuItems.GetRange(0, MenuItems.Count - GetDefaultChoicesCount() + 1);
            //MenuItems = new List<MenuItem>();
           
            _shouldRemoveLastMenuItem = true;
        }

        public void ModifyMenuItem(string userChoice, string newUserChoice, string newLabel = "")
        {
            var menuItem = MenuItems.Find(item => item.UserChoice == userChoice);
            menuItem.UserChoice = newUserChoice;
            

            if (DefaultMenuItems.Contains(userChoice))
            {
                var index = DefaultMenuItems.IndexOf(userChoice);
                DefaultMenuItems[index] = newUserChoice;
            }
            menuItem.Label = string.IsNullOrWhiteSpace(newLabel) ? menuItem.Label : newLabel;
        }

        public ExecutionResults RunMenu()
        {
            if (_shouldRemoveLastMenuItem) MenuItems.RemoveAt(MenuItems.Count - 1);
            var listener = new InputListener();
            
            var command = NavigationCommand.Stay;
            var currentLine = 0; // start from first MenuItem, not from header
            var userChoice = "";
            bool isCommandToLoadGame = false;
            var strValue = "";
            while (!IsEscapeMenuCommand(command))
            {
                View.DrawMenu(currentLine, MenuItems, userChoice, Title);
                
                var (result, line, reDraw) = listener.Listen(currentLine, MenuItems.Count, userChoice);
                var isDown = line > currentLine; // needed to skip separators
                currentLine = line;
                if (currentLine < MenuItems.Count && IsMenuSeparator(currentLine))
                {
                    currentLine = isDown ? currentLine + 1 : currentLine - 1;
                    userChoice = IsInRange(currentLine) ? MenuItems[currentLine].UserChoice : "";
                    continue;
                }

                userChoice = result != "" ? result 
                    : IsInRange(currentLine) ? MenuItems[currentLine].UserChoice
                    : MenuItems[0].UserChoice;

                if (reDraw) continue;

                var newCommand = command;
                var menuItem = MenuItems.Find(item => item.UserChoice == userChoice);
                if (menuItem != null)
                {
                    var executionResults = menuItem.MethodToExecute();
                    newCommand = executionResults.Command;
                    isCommandToLoadGame = executionResults.ShouldLoadGame;
                    strValue = executionResults.StrValue;
                }
                else
                {
                    View.DrawInvalidInputMessage(userChoice, Title);
                }
                
                command = newCommand;
                userChoice = "";
            }

            var results = new ExecutionResults
            {
                ShouldLoadGame = isCommandToLoadGame,
                StrValue = strValue
            };
            
            switch (command)
            {
                case NavigationCommand.Exit:
                {
                    if (_menuLevel != 0) return results.WithCommand(NavigationCommand.Exit);
                    View.ClearAndDrawHeader(Title);
                    Console.WriteLine("Closing down...");
                    return results.WithCommand(NavigationCommand.Exit);
                }
                case NavigationCommand.ToMain:
                    return results.WithCommand(NavigationCommand.ToMain);
                default:
                    return results.WithCommand(NavigationCommand.Stay);
            }
        }

        private bool IsEscapeMenuCommand(NavigationCommand command)
        {
            return command == NavigationCommand.Exit
                   || command == NavigationCommand.ToMain && _menuLevel != MenuLevel.Level0
                   || command == NavigationCommand.ToPrevious && _menuLevel == MenuLevel.Level2Plus;
        }

        private bool IsMenuSeparator(int line)
        {
            return line >= 0 && MenuItems[line].GetType() == typeof(MenuItemSeparator);
        }

        private bool IsInRange(int line)
        {
            return line >= 0 && line < MenuItems.Count;
        }
    }
}