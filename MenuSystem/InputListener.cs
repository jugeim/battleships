﻿using System;
using System.Linq;

namespace MenuSystem
{
    public class InputListener
    {
        private readonly ConsoleKey[] _disabledKeys =
        {
            ConsoleKey.Delete,
            ConsoleKey.LeftArrow,
            ConsoleKey.RightArrow,
            ConsoleKey.Insert,
            ConsoleKey.Home,
            ConsoleKey.PageUp,
            ConsoleKey.PageDown,
            ConsoleKey.End,
            ConsoleKey.F1,
            ConsoleKey.F2,
            ConsoleKey.F3,
            ConsoleKey.F4,
            ConsoleKey.F5,
            ConsoleKey.F6,
            ConsoleKey.F7,
            ConsoleKey.F8,
            ConsoleKey.F8,
            ConsoleKey.F9,
            ConsoleKey.F10,
            ConsoleKey.F11,
            ConsoleKey.F12,
        };

        public (string result, int line, bool reDraw) Listen(int currentLine, int height, string prevInput)
        {
            var result = prevInput;
            var line = currentLine;
            while (true)
            {
                var keyInfo = Console.ReadKey();
                
                if (_disabledKeys.Contains(keyInfo.Key)) continue;

                switch (keyInfo.Key)
                {
                    case ConsoleKey.Enter:
                        return (result, line, false);
                    case ConsoleKey.UpArrow:
                        line = line - 1 > -1 ? line - 1 : height - 1;
                        return ("", line, true);
                    case ConsoleKey.DownArrow:
                        line = line + 1 < height ? line + 1 : 0;
                        return ("", line, true);
                    case ConsoleKey.Backspace when result == "":
                        return ("r", line, false);
                    case ConsoleKey.Backspace:
                        Console.Write(" \b \b");
                        result = result.Substring(0, result.Length - 1);
                        break;
                    default:
                    {
                        if (line < height) return (keyInfo.KeyChar.ToString(), height, true);
                        result += keyInfo.KeyChar;
                        break;
                    }
                }
            }
        }
    }
}