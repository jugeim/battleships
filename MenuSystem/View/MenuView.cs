﻿using System;
using System.Collections.Generic;
using System.Threading;
using MenuSystem.MenuItems;

namespace MenuSystem.View
{
    public class MenuView
    {
        public virtual void DrawMenu(int selectedLine, List<MenuItem> items, string prevInput, string menuTitle)
        {
            ClearAndDrawHeader(menuTitle);

            foreach (var menuItem in items)
            {
                if (menuItem.GetType() == typeof(MenuItemCheckbox))
                {
                    var menuCheckbox = (MenuItemCheckbox) menuItem;
                    Console.ForegroundColor = menuCheckbox.IsSelected() ? ConsoleColor.Green : ConsoleColor.Red;
                }
                Console.WriteLine(menuItem);
                Console.ForegroundColor = ConsoleColor.Gray;
            }

            var toWrite = $">{prevInput}";
            Console.Write(toWrite);
            Console.SetCursorPosition(selectedLine == items.Count ? toWrite.Length : 0, selectedLine + 1);
        }
        public void ClearAndDrawHeader(string title)
        {
            Console.Clear();
            Console.WriteLine($"==========> {title} <==========");
        }

        public void DrawInvalidInputMessage(string input,string menuTitle)
        {
            ClearAndDrawHeader(menuTitle);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"Invalid input \"{input}\", try again");
            Console.ForegroundColor = ConsoleColor.White;
            Thread.Sleep(800);
        }
    }
}