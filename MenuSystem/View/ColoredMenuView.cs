﻿using System;
using System.Collections.Generic;
using MenuSystem.MenuItems;

namespace MenuSystem.View
{
    public class ColoredMenuView : MenuView
    {
        private readonly ConsoleColor _baseColor;
        private readonly ConsoleColor _selectionColor;
        
        public ColoredMenuView(ConsoleColor baseColor = ConsoleColor.DarkMagenta, ConsoleColor selectionColor = ConsoleColor.Cyan)
        {
            _baseColor = baseColor;
            _selectionColor = selectionColor;
        }
        
        public override void DrawMenu(int selectedLine, List<MenuItem> items, string currentInput, string menuTitle)
        {
            var defaultColor = Console.ForegroundColor;
            Console.ForegroundColor = _baseColor;
            ClearAndDrawHeader(menuTitle);

            for (var i = 0; i < items.Count; i++)
            {
                if (i == selectedLine) Console.ForegroundColor = _selectionColor;

                Console.WriteLine(items[i]);
                Console.ForegroundColor = _baseColor;
            }

            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.Write($"{currentInput.ToUpper()}");
            Console.ForegroundColor = defaultColor;
        }
    }
}