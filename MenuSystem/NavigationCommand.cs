﻿namespace MenuSystem
{
    public enum NavigationCommand
    {
        Stay,
        ToPrevious,
        ToMain,
        Exit
    }
}